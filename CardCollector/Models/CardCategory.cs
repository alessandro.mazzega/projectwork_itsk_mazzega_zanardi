﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    ///<summary>
    ///Class used to represent a N:N relation between the entities Card and Category. It indicates that a card is of a certain category. Categories are exclusive for each game.
    ///</summary>  
    public class CardCategory {
        
        public int CardCategoryId { get; set; }
        public int CardId { get; set; }
        public int CategoryId { get; set; }
        public Card Card { get; set; }
        public Category Category { get; set; }

        public CardCategory(int categoryId, int cardId) {
            this.CardId = cardId;
            this.CategoryId = categoryId;
        }
    }
}
