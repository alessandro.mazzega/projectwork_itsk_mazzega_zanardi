﻿using System.Collections.Generic;

namespace CardCollector.Models {
    /// <summary>
    /// Decks are composition of more cards, in which there can be more copies of the same card.
    /// </summary>
    public class Deck {
        public int DeckId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public int CardCount { get; set; }
        /// <summary>
        /// Value: true = this deck can be used to play
        /// </summary>
        public int UserId { get; set; }
        public Game Game { get; set; }

        public List<DeckCard> DeckList { get; set; }
    }
}
