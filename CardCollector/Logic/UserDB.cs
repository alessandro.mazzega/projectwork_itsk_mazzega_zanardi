﻿using CardCollector.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Logic {
    public class UserDB {

        /// <summary>
        /// Fetch all the users from the db
        /// </summary>
        /// <returns>A list of all the users in the db</returns>
        public static List<User> GetUsers() {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"select * from [User]";
                return db.Query<User>(sqlQuery).ToList();
            }
        }
        /// <summary>
        /// Fetch a specific user from the db
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns>Desired user or NULL if not found</returns>
        public static User GetUser(int userId) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"SELECT * FROM [User] WHERE userId = @userId";
                return db.Query<User>(sqlQuery, new { userId = userId }).FirstOrDefault();
            }
        }

        /// <summary>
        /// Adds a user with basic permissions
        /// </summary>
        /// <param name="user">User data from the web app</param>
        /// <returns>Id of the inserted user</returns>
        public static int AddUser(User user) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"INSERT INTO [User] VALUES (@username, @password, @email, 2)";
                try {
                    db.Execute(sqlQuery, new { username = user.Username, password = user.Password, email = user.Email });
                    sqlQuery = "SELECT TOP 1 * FROM [User] WHERE username = @username";
                    return db.Query<User>(sqlQuery, new { username = user.Username }).FirstOrDefault().UserId;
                } catch (Exception) {
                    return -1;
                }
            }
        }

        /// <summary>
        /// Delete a user from the db
        /// </summary>
        /// <param name="id"></param>
        /// <returns>TRUE if the object was deleted, FALSE if a problem occurred</returns>
        public static bool DeleteUser(int id) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {

                try {
                    string sqlQuery = $"DELETE FROM UserCard WHERE UserId = @userId";
                    db.Query(sqlQuery, new { userId = id });
                    sqlQuery = $"DELETE FROM Deck WHERE UserId = @userId";
                    db.Query(sqlQuery, new { userId = id });
                    sqlQuery = $"DELETE FROM [User] WHERE UserId = @userId";
                    db.Query(sqlQuery, new { userId = id });
                    return true;
                } catch (Exception) {
                    return false;
                }
            }
        }

        /// <summary>
        /// checks if the user credentials are present in the database
        /// </summary>
        /// <param name="user">data of the user that is trying to login</param>
        /// <returns>TRUE if the login credentials were correct, FALSE if they were not</returns>
        public static User TryLogin(User user) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"SELECT * FROM [User] WHERE UserName = @username AND Password = @password";
                user = db.Query<User>(sqlQuery, new { @username = user.Username, @password = user.Password }).FirstOrDefault();
                return user;
            }
        }

        /// <summary>
        /// Checks whether a certain username is present in the db
        /// </summary>
        /// <param name="username">Username to be searched in the db</param>
        /// <returns>TRUE if the name is NOT in the db</returns>
        public static bool CheckUsername(string username) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"SELECT * FROM [User] WHERE Username = @username";
                User user = db.Query<User>(sqlQuery, new { username = username }).FirstOrDefault();
                if (user == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Checks whether a certain email is present in the database
        /// </summary>
        /// <param name="email">Email to be searched in the db</param>
        /// <returns>TRUE if the email is NOT in the db</returns>
        public static bool CheckMail(string email) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = $"SELECT * FROM [User] WHERE Email = @email";
                User user = db.Query<User>(sqlQuery, new { email = email }).FirstOrDefault();
                if (user == null)
                    return false;
                else
                    return true;
            }
        }
    }

    public class TempUser {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public int Role { get; set; }
    }
}
