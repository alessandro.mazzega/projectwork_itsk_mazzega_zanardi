import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { ApiService, UserService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { UserCard } from 'src/app/models/userCard.model';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-card-list',
  templateUrl: 'card-list.component.html',
  styles: []
})

export class CardListComponent implements OnInit {
  userId: number;
  dataLoaded: boolean;
  cardList: Card[];
  shownCards: Card[];
  game: number;
  showFilter: boolean;
  showGame: boolean;
  toggledCard: Card;

  constructor(private srv: ApiService, private apiSrv: ApiService, private router: Router) {

  }

  ngOnInit() {
    this.userId = +localStorage.getItem("user");
    //set initial values
    this.toggledCard = new Card();
    this.dataLoaded = false;
    this.showFilter = false;
    this.showGame = false;
    //get the content
    this.loadCards();
    this.dataLoaded = true;
  }


  //gets the list of cards and if a user is logged in adds the copies owned for each card
  loadCards() {
    //get the whole list of cards
    this.srv.GetCards().subscribe(cards => {
      //sort the cards for faster search
      cards.sort((a, b) => { return a.cardId - b.cardId });
      this.cardList = cards;
      //add the copies owned by the user, if logged in
      if (this.userId != null) {
        let userCard: UserCard = {
          gameId: +localStorage.getItem("game"),
          userId: this.userId,
          cardId: -1,
          copies: null,
          user: null,
          card: null
        };
        this.srv.GetCopies(userCard).subscribe(userCards => {
          userCards.sort((a, b) => { return a.cardId - b.cardId });
          userCards.forEach(userC => {
            let completed: boolean = false;
            let index: number = 0;
            while (!completed) {
              if (userC.cardId == this.cardList[index].cardId) {
                //the card is in the user collection, search over
                this.cardList[index].copies = userC.copies;
                completed = true;
              } else if (userC.cardId < this.cardList[index].cardId) {
                //the cards was not found, search over
                completed = true;
              } else {
                //searching...
                index++;
              }
            }
          });

          //add the copies to each card
          this.cardList.forEach(card => {
            userCards.forEach(userC => {
              userC.cardId == card.cardId ? card.copies = userC.copies : card.copies = 0;
            });
            let userCard: UserCard = userCards.find(x => card.cardId == x.cardId);
            userCard == null ? card.copies = 0 : card.copies = userCard.copies;
          });
          this.shownCards = this.cardList;
        });
      }
    });

  }

  toggleDetail($event) {
    this.toggledCard = $event;
  }

  onToggleFilter() {
    this.showFilter == true ? this.showFilter = false : this.showFilter = true;
  }

  onToggleGame() {
    this.showGame == true ? this.showGame = false : this.showGame = true;
  }

  filterCards($event: Card[]) {
    this.shownCards = $event;
    this.shownCards.forEach(card => {
      this.cardList[this.cardList.findIndex(x => x.cardId == card.cardId)].copies = card.copies;
    });
    this.onToggleFilter();
  }

  changeGame($event) {
    localStorage.setItem("game", $event);
    this.onToggleGame();
    this.ngOnInit();
  }

  saveChanges() {
    //get the list of cards that were modified
    let cards: Card[] = this.cardList.filter(x => x.copies != 0);
    let userCards: UserCard[] = [];
    cards.forEach(card => {
      //create new usercard to store the changes
      let userCard: UserCard = {
        cardId: card.cardId,
        userId: this.userId,
        gameId: +localStorage.getItem("game"),
        copies: card.copies,
        user: null,
        card: null
      }
      userCards.push(userCard);
    });
    this.apiSrv.UpdateCollection(userCards).subscribe(res => {
      console.log(res);
      this.ngOnInit();
    });
    //send the list to the server
  }

  updateCard($event: Card) {
    this.cardList[this.cardList.findIndex(x => x.cardId == $event.cardId)].copies = $event.copies;
  }
}
