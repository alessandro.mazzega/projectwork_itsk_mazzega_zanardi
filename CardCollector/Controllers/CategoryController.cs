﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        // GET: api/Category
        [HttpGet]
        public List<Category> Get() {
            return GenericDB.GetFromDb<Category>("Category");
        }

        // GET: api/Category/5
        [HttpGet("{id}")]
        public Category Get(int id) {
            return GenericDB.GetFromDb<Category>("Category", id);
        }

        // POST: api/Category
        [HttpPost]
        public IActionResult Post() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // PUT: api/Category/5
        [HttpPut("{id}")]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
