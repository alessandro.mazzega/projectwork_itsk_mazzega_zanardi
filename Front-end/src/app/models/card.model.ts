import { Category } from './category.model';
import { Attribute } from './attribute.model';
import { Type } from './type.model';
import { Rarity } from './rarity.model';

export class Card {
  cardId: number;
  paperId: string;
  gameId: number;
  name: string;
  rarityId: number;
  image: string;
  cost: string;
  copies: number;
  categories: Category[];
  attributes: Attribute[];
  types: Type[];
  rarity: Rarity;

}