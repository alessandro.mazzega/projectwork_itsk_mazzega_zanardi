import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'clean'
})
export class CleanCostPipe implements PipeTransform {

  transform(value: string): string {
    value = value.replace(/,/g, " | ");
    value = value.replace(/{/g, "");
    value = value.replace(/}/g, "");
    return value;
  }
}
