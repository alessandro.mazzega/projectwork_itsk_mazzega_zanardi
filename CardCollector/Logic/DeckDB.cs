﻿using CardCollector.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace CardCollector.Logic {
    public class DeckDB {

        /// <summary>
        /// Returns the deck corresponding to a certain id or null if not found
        /// </summary>
        /// <param name="deckId"></param>
        /// <returns></returns>
        public static Deck GetDeck(int deckId) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {
                string sqlQuery = $"SELECT * FROM Deck WHERE deckId = @deckId";
                Deck result = db.Query<Deck>(sqlQuery, new { deckId = deckId }).FirstOrDefault();
                result.Game = GenericDB.GetGame(result.GameId);
                result.DeckList = GetDecklist(result);
                return result;
            }
        }


        /// <summary>
        /// Adds a deck to a certain user library
        /// </summary>
        /// <param name="deckName">Name to be given to the new deck</param>
        /// <param name="gameId">Id of the game for which the deck is meant for</param>
        /// <param name="userId">Id of the user that owns the deck</param>
        /// <returns>The id of the deck in the database</returns>
        public static int AddDeck(string deckName, int gameId, int userId) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {
                string sqlQuery =   "INSERT INTO Deck VALUES (@gameId, 0, @deckName, @userId)" +
                                    "SELECT CAST(SCOPE_IDENTITY() as int)";
                try {
                    return db.Query<int>(sqlQuery, new { gameId = gameId, deckName = deckName, userId = userId }).Single();

                } catch (Exception) {

                    return -1;
                }
            }
        }

        /// <summary>
        /// Remove a deck from a certain user library
        /// </summary>
        /// <param name="deckId">Id of the deck to be removed</param>
        /// <returns>TRUE if the deck was removed successfully, FALSE if the operation was not succesful</returns>
        public static bool RemoveDeck(int deckId) {

            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {

                try {
                    //remove the cards from the deck
                    string sqlQuery = $"DELETE DeckCard WHERE deckId = @deckId";
                    db.Execute(sqlQuery, new { deckId = deckId });

                    //remove the deck from the list
                    sqlQuery = $"DELETE Deck WHERE deckId = @deckId";

                    db.Execute(sqlQuery, new { deckId = deckId });
                    return true;
                } catch (Exception e) {
                    return false;
                }

            }
        }

        /// <summary>
        /// Returns the list of cards in the deck.
        /// </summary>
        /// There could be data inconsistency, therefore the linked data should not be considered correct at all times.
        /// <param name="deck">Deck for which the decklist is required</param>
        /// <returns>The list of cards contained in the deck</returns>
        public static List<DeckCard> GetDecklist(Deck deck) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "Select * From [DeckCard] Where DeckId = @deckId";
                List<DeckCard> deckList = db.Query<DeckCard>(sqlQuery, new { deckId = deck.DeckId }).ToList();
                //get the details for every card in the deck
                foreach (DeckCard deckCard in deckList) {
                    deckCard.Card = CardDB.GetCard(deckCard.CardId);
                    deckCard.Game = deck.Game;
                }
                return deckList;
            }
        }

        public static bool UpdateDecklist(List<DeckCard> deckList) {
            if (deckList.Count <= 0) return true;
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "IF EXISTS "
                    + " (SELECT * FROM DeckCard WHERE deckId = @deckId AND cardId = @cardId)    "
                    + "   UPDATE DeckCard SET copies = @copies                                "
                    + "   WHERE deckId = @deckId AND cardId = @cardId                            "
                    + " ELSE "
                    + "   INSERT INTO DeckCard VALUES (@deckId, @cardId, @copies, @gameId) ";
                //add the new cards to the deck and update the numbers of the existing ones
                List<int> cardIds = new List<int>();
                try {
                    foreach (DeckCard deckCard in deckList) {
                        db.Execute(sqlQuery, new { deckId = deckCard.DeckId, cardId = deckCard.CardId, copies = deckCard.Copies, gameId = deckCard.GameId });
                        cardIds.Add(deckCard.CardId);
                    }
                } catch (Exception e) {
                    return false;
                }
                //remove the cards that are not in the deck anymore
                try {
                    sqlQuery = "DELETE FROM DeckCard WHERE deckId = @deckId AND NOT (cardId IN @cardIds) ";
                    db.Execute(sqlQuery, new { deckId = deckList[0].DeckId, cardIds = cardIds });
                } catch (Exception e) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Get the decks owned by a certain user
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <returns>List of decks owned by the user</returns>
        public static List<Deck> GetDecksByUser(int userId) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {
                string sqlQuery = $"select * from Deck where userId = @userId";

                List<Deck> decks = db.Query<Deck>(sqlQuery, new { userId = userId }).ToList();
                foreach (Deck deck in decks) {
                    deck.Game = GenericDB.GetGame(deck.GameId);
                    deck.DeckList = GetDecklist(deck);
                }

                return decks;
            }
        }

        /// <summary>
        /// Exports the deck in a format that is easy to share
        /// </summary>
        /// <param name="deckId">Id of the deck to be exported</param>
        /// <returns>String containing the necessary information about the deck</returns>
        public static string ExportDeck(int deckId) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {
                string result = "";

                string sqlQuery = $"SELECT * FROM Deck WHERE deckId = @deckId";
                try {

                    Deck deck = db.Query<Deck>(sqlQuery, new { deckid = deckId }).FirstOrDefault();
                    result += deck.Name + "\n" + deck.GameId + "\n";

                    //getting the cards in the deck
                    sqlQuery = $"SELECT * FROM DeckCard WHERE deckId = @deckId";
                    List<DeckCard> deckCards = db.Query<DeckCard>(sqlQuery, new { deckId = deckId }).ToList();
                    foreach (DeckCard deckCard in deckCards) {
                        sqlQuery = "SELECT Name FROM Card WHERE cardId = @cardId";
                        string name = db.Query<string>(sqlQuery, new { cardId = deckCard.CardId }).FirstOrDefault();
                        result += $"{deckCard.Copies}x {name}\n";
                    }
                } catch (Exception e) { }


                return result;
            }
        }

        /// <summary>
        /// Imports a deck exported with the ExportDeck method (or formatted accordingly) to the user library
        /// </summary>
        /// <param name="deckString">String containing the deck</param>
        /// <param name="userId">Id of the user importing the deck</param>
        public static int ImportDeck(string deckString, int userId) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {

                string sqlQuery = "";
                Deck deck = new Deck();

                using (StringReader reader = new StringReader(deckString)) {
                    try {


                        //get header of the deck
                        string line = reader.ReadLine();
                        deck.Name = line;
                        deck.GameId = Convert.ToInt32(reader.ReadLine());
                        deck.UserId = userId;



                        //get list of cards
                        List<int> cardIds = new List<int>();
                        List<int> copies = new List<int>();
                        int cardCount = 0;
                        sqlQuery = $"SELECT CardId FROM Card WHERE GameId = @gameId AND name = @name";
                        while ((line = reader.ReadLine()) != null) {

                            copies.Add(Convert.ToInt32(line.Split("x ").First()));
                            cardCount += copies.Last();
                            //get the name of the card
                            //the name starts 2 characters after the number. So number length + 2 = offset
                            line = (line.Substring(line.Split("x ").First().Length + 2));
                            cardIds.Add(db.Query<int>(sqlQuery, new { gameId = deck.GameId, name = line }).FirstOrDefault());
                        }


                        //add deck to db
                        sqlQuery =
                            "INSERT INTO Deck VALUES (@gameId, @cardCount, @name, @userId) "
                            + " SELECT CAST(SCOPE_IDENTITY() as int)";
                        int deckId = db.Query<int>(sqlQuery, new { gameId = deck.GameId, cardCount = cardCount, name = deck.Name, userId = userId }).First();

                        sqlQuery = "INSERT INTO DeckCard VALUES (@deckId, @cardId, @copies, @gameId)";
                        for (int i = 0; i < cardIds.Count; i++) {
                            db.Execute(sqlQuery, new { deckId = deckId, cardId = cardIds[i], copies = copies[i], gameId = deck.GameId });
                        }
                        return deckId;
                    } catch (Exception e) {
                        return -1;
                    }

                }

            }
        }
    }

    public class TempDeckCard {
        public int Copies { get; set; }
        public string Name { get; set; }
        public int GameId { get; set; }
    }
}
