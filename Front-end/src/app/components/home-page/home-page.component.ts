import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/services';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private apiSrv: ApiService) { }

  ngOnInit() {

  }


  toTheBatcave(){
    console.log("TO THE BATCAVE");
    this.router.navigate(["card-list"]);
  }

}
