import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Card } from "../models/card.model";
import { Attribute } from "../models/attribute.model";
import { Type } from "../models/type.model";
import { Category } from "../models/category.model";
import { Rarity } from "../models/rarity.model";
import { Game } from "../models/game.model";
import { User } from "../models/user.model";
import { Deck } from "../models/deck.model";
import { UserCard } from '../models/userCard.model';
import { DeckCard } from '../models/deckCard.model';

@Injectable({
  providedIn: "root"
})
export class ApiService {
  private baseUrl = "https://localhost:44328/api/";
  constructor(private http: HttpClient) { }

  GetCards() {
    let gameId = +localStorage.getItem("game");
    if (gameId === 0) {
      return this.http.get<Card[]>(this.baseUrl + "Card");
    } else {
      return this.http.get<Card[]>(this.baseUrl + "Card/Game/" + gameId);
    }
  }

  GetFilteredCards(filter: tempCard) {
    let gameId = +localStorage.getItem("game");
    if (gameId === 0) {
      return this.http.post<Card[]>(this.baseUrl + "Card", filter);
    } else {
      return this.http.post<Card[]>(this.baseUrl + "Card/Filter", filter);
    }
  }

  GetCopies(userCard: UserCard) {
    return this.http.post<UserCard[]>(this.baseUrl + "Card/Copies", userCard);
  }

  GetGames() {
    return this.http.get<Game[]>(this.baseUrl + "Game");
  }

  GetGame(id: number) {
    return this.http.get<Game[]>(this.baseUrl + "Game/" + id);
  }

  GetAttributes() {
    return this.http.get<Attribute[]>(this.baseUrl + "Attribute");
  }

  GetType() {
    return this.http.get<Type[]>(this.baseUrl + "TypeCard");
  }

  GetCategories() {
    return this.http.get<Category[]>(this.baseUrl + "Category");
  }

  GetRarities() {
    return this.http.get<Rarity[]>(this.baseUrl + "Rarity");
  }

  UpdateCollection(cardlist: UserCard[]) {
    return this.http.put<boolean>(this.baseUrl + "Card/Collection", cardlist);
  }
}

export class tempCard {
  cardId: number;
  paperId: string;
  gameId: number;
  name: string;
  rarityId: number;
  image: string;
  cost: string;

  categories: Category[];
  attributes: Attribute[];
  types: Type[];
  rarities: Rarity[];
  userId: number;
}

export class DeckService {
  private baseUrl = "https://localhost:44328/api/Deck/";
  constructor(private http: HttpClient) { }

  GetDecks() {
    return this.http.get<Deck[]>(this.baseUrl);
  }

  GetDeck(deckId: number) {
    return this.http.get<Deck>(this.baseUrl + deckId);
  }


  GetDeckList(deck: Deck) {
    return this.http.post<DeckCard[]>(this.baseUrl + 'DeckList', deck);
  }

  SendDeckList(deckList: DeckCard[]) {
    return this.http.put<boolean>(this.baseUrl + "DeckList", deckList);
  }

  DeleteDeck(deckId: number) {
    return this.http.delete(this.baseUrl + deckId);
  }

  CreateDeck(deck: Deck) {
    return this.http.put<number>(this.baseUrl, deck)
  }

  ImportDeck(deckString: string[], userId: number){
    return this.http.put<number>(this.baseUrl + "import/" + userId, deckString);
  }

  ExportDeck(deckId: number){
    return this.http.get<any>(this.baseUrl + "export/" + deckId);
  }
}

export class UserService {
  private baseUrl = "https://localhost:44328/api/User/";
  constructor(private http: HttpClient) { }

  TryLogin(user: User) {
    return this.http.post<User>(this.baseUrl + "Login", user);
  }

  Signup(user: User) {
    return this.http.put<number>(this.baseUrl + "Signup", user);
  }

  EmailAvailable(email: string) {
    return this.http.post<boolean>(this.baseUrl + "Email", email);
  }

  UsernameAvailable(username: string) {
    return this.http.post<boolean>(this.baseUrl + "Username", username);
  }

  GetUser(userId: number){
    return this.http.get<User>(this.baseUrl + userId);
  }
}


