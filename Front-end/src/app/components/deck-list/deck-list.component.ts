import { Component, OnInit, OnChanges } from '@angular/core';
import { ApiService, UserService, DeckService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { Deck } from 'src/app/models/deck.model';
import { DeckCard } from 'src/app/models/deckCard.model';


@Component({
  selector: 'app-deck-list',
  templateUrl: 'deck-list.component.html',
  styleUrls: ["deck-list.component.css"]
})
export class DeckListComponent implements OnInit {

  public opened = false;
  searchText: string;
  decks: Deck[];
  shownDecks: Deck[];
  showDeck: boolean;
  game: number;
  selectedDeck: Deck;
  deckList: DeckCard[];
  userId: number;
  constructor(private apiSrv: ApiService, private deckSrv: DeckService, private router: Router) { }

  async ngOnInit() {
    this.userId = +localStorage.getItem("user");
    this.selectedDeck = new Deck();
    this.selectedDeck.deckList = [];
    this.searchText = "";
    this.decks = await this.deckSrv.GetDecks().toPromise();
    this.decks = this.decks.filter(x => x.userId == this.userId);
    this.shownDecks = this.decks;
  }

  OnToggleWindow() {
    this.opened = !this.opened;
  }

  public close() {
    this.opened = false;
  }

  search() {
    if (this.searchText.length <= 0) {
      this.shownDecks = this.decks;
    } else {
      this.shownDecks = this.decks.filter(x => x.name.includes(this.searchText));
    }
  }

  async loadDeck(deck: Deck) {
    this.selectedDeck = deck;
    this.selectedDeck.deckList = await this.deckSrv.GetDeckList(deck).toPromise();
  }

  editDeck(deckId: number) {
    console.log("navigating");
    this.router.navigate(["editor", deckId]);
  }

  importDeck() {
    navigator.clipboard.readText().then(async text => {
      var words = text.split("/\r?\n/");
      console.log(words);
      var result = await this.deckSrv.ImportDeck(words, +localStorage.getItem("user")).toPromise();
      console.log(result);
      this.router.navigate(["editor", result]);
    }).catch(err => {
      console.log('Something went wrong', err);
    });
  }

  async exportDeck(deckId: number) {
    let result = { text: "" };
    result = await this.deckSrv.ExportDeck(deckId).toPromise();
    console.log(result.text)
    navigator.clipboard.writeText(result.text).catch(err => {
      console.log('Something went wrong', err);
    });
    window.alert("The deck has been exported to your clipboard");
  }


  async deleteDeck(deckId: number) {
    if (window.confirm('Are you sure you want to delete ' + this.decks.find(x => x.deckId == deckId).name + '?')) {
      var res = await this.deckSrv.DeleteDeck(deckId).toPromise();
      console.log(res);
      if (res) {
        window.alert('you have successfully deleted your deck!');
        this.decks.splice(this.decks.findIndex(x => x.deckId == deckId), 1);
        this.selectedDeck = new Deck();
        this.shownDecks = this.decks;
      } else {
        //maybe use an error notification
        window.alert('I have a problem deleting the deck')
      }
    }
  }
}
