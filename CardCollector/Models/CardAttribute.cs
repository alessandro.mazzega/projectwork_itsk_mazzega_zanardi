﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// Class used to represent a N:N relation between the entities Card and Attribute.  The relation identifies the type of a certain card.
    /// </summary>
    public class CardAttribute {
        public int CardAttributeId { get; set; }
        public int CardId { get; set; }
        public int AttributeId { get; set; }
        public Card Card { get; set; }
        public Attribute Attribute { get; set; }

        public CardAttribute(int cardId, int attributeId) {
            this.CardId = cardId;
            this.AttributeId = attributeId;
        }   
    }
}
