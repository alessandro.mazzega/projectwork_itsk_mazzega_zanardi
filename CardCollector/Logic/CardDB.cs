﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using CardCollector.Models;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;

namespace CardCollector.Logic {
    public class CardDB {
        /// <summary>
        /// Gets the list of card from the database
        /// </summary>
        /// <returns>
        /// List</returns>
        public static List<Card> GetCards() {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "SELECT * FROM Card";
                List<Card> cards = db.Query<Card>(sqlQuery).ToList();

                foreach (Card card in cards) {
                    //get attributes
                    sqlQuery = "select A.AttributeId, A.Name, A.GameId from Attribute A join CardAttribute CA on A.AttributeId = CA.AttributeId where CA.CardId = @cardId";
                    card.Attributes = db.Query<Models.Attribute>(sqlQuery, new { cardId = card.CardId }).ToList();
                    //get categories
                    sqlQuery = "select C.CategoryId, C.Name, C.GameId from Category C join CardCategory CC on C.CategoryId = CC.CategoryId where CC.CardId = @cardId";
                    card.Categories = db.Query<Models.Category>(sqlQuery, new { cardId = card.CardId }).ToList();
                    //get types
                    sqlQuery = "select T.TypeId, T.Name, T.GameId from TypeCard T join CardsTypeCard CT on T.TypeId = CT.TypeId where CT.CardId = @cardId";
                    card.Types = db.Query<TypeCard>(sqlQuery, new { cardId = card.CardId }).ToList();
                    card.Image = ConvertImage(card.Image);
                }
                return cards;
            }
        }

        /// <summary>
        /// Get a specific card from DB
        /// </summary>
        /// <param name="id">id of the card to be returned</param>
        /// <returns>Null if not found, and the desired card in the other cases</returns>
        public static Card GetCard(int id) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "Select * From [Card] Where CardId = @cardId";
                Card card = db.Query<Card>(sqlQuery, new { cardId = id }).FirstOrDefault();

                if (card != null) {
                    //get attributes
                    sqlQuery = "select A.AttributeId, A.Name, A.GameId from Attribute A join CardAttribute CA on A.AttributeId = CA.AttributeId where CA.CardId = @cardId";
                    card.Attributes = db.Query<Models.Attribute>(sqlQuery, new { cardId = id }).ToList();
                    //get categories
                    sqlQuery = "select C.CategoryId, C.Name, C.GameId from Category C join CardCategory CC on C.CategoryId = CC.CategoryId where CC.CardId = @cardId";
                    card.Categories = db.Query<Models.Category>(sqlQuery, new { cardId = id }).ToList();
                    //get types
                    sqlQuery = "select T.TypeId, T.Name, T.GameId from TypeCard T join CardsTypeCard CT on T.TypeId = CT.TypeId where CT.CardId = @cardId";
                    card.Types = db.Query<TypeCard>(sqlQuery, new { cardId = id }).ToList();
                }

                card.Image = ConvertImage(card.Image);
                return card;
            }
        }

        public static List<Card> GetCardsByGame(int gameId) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "SELECT * FROM [Card] Where GameId = @gameId";
                List<Card> cards = db.Query<Card>(sqlQuery, new { gameId = gameId }).ToList();
                foreach (Card card in cards) {
                    //get attributes
                    sqlQuery = "select A.AttributeId, A.Name, A.GameId from Attribute A join CardAttribute CA on A.AttributeId = CA.AttributeId where CA.CardId = @cardId";
                    card.Attributes = db.Query<Models.Attribute>(sqlQuery, new { cardId = card.CardId }).ToList();
                    //get categories
                    sqlQuery = "select C.CategoryId, C.Name, C.GameId from Category C join CardCategory CC on C.CategoryId = CC.CategoryId where CC.CardId = @cardId";
                    card.Categories = db.Query<Models.Category>(sqlQuery, new { cardId = card.CardId }).ToList();
                    //get types
                    sqlQuery = "select T.TypeId, T.Name, T.GameId from TypeCard T join CardsTypeCard CT on T.TypeId = CT.TypeId where CT.CardId = @cardId";
                    card.Types = db.Query<TypeCard>(sqlQuery, new { cardId = card.CardId }).ToList();
                    card.Image = ConvertImage(card.Image);
                }
                return cards;

            }
        }

        /// <summary>
        /// Get a card from its name
        /// </summary>
        /// <param name="cardName">Name of the card to be fetched</param>
        /// <returns>Null if not found, </returns>
        public static Card SearchCard(string cardName) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "Select CardId From [Card] Where [Name] = @name";
                int id = db.Query<int>(sqlQuery, new { name = cardName }).FirstOrDefault();
                return GetCard(id);
            }
        }

        /// <summary>
        /// Get the list of cards that satisfy the condition of the filter
        /// </summary>
        /// For a card to show up in the result, it must possess every non-null value from the filter
        /// <param name="card">Filter for card-specific details</param>
        /// <param name="attributes">Filter for the attributes</param>
        /// <param name="categories">Filter for the categories</param>
        /// <param name="types">Filter for the types</param>
        /// <param name="rarities">Filter for the rarities</param>
        /// <returns>The list of filtered cards</returns>
        public static List<Card> FilterCards(Card card, List<Models.Attribute> attributes, List<Category> categories, List<TypeCard> types, List<Rarity> rarities, int gameId) {
            List<Card> cards = new List<Card>();
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                //first we need to find the id of the cards that fit the filter
                string withClause = "";
                string sqlQuery = " SELECT [Card].*   ";
                string fromClause = " FROM [Card] ";
                List<int> attributeIds = new List<int>();
                List<int> categoryIds = new List<int>();
                List<int> typeIds = new List<int>();
                List<int> rarityIds = new List<int>();

                //attribute subquery
                if (attributes != null) {
                    fromClause += " JOIN FilteredByAttribute fa ON Card.CardId = fa.CardId ";
                    attributeIds = attributes.Select(x => x.AttributeId).ToList();
                    string attributeStatement =
                        " FilteredByAttribute (CardId) AS   " +
                        " (SELECT DISTINCT CardId                " +
                        " FROM CardAttribute                    " +
                        " WHERE AttributeId IN @attributeIds )  ";
                    if (string.IsNullOrEmpty(withClause)) {
                        withClause += "WITH " + attributeStatement;
                    } else {
                        withClause +=  ", " + attributeStatement;
                    }
                }

                //category subquery
                if (categories != null) {
                    fromClause += " JOIN FilteredByCategory fc ON Card.CardId = fc.CardId ";
                    categoryIds = categories.Select(x => x.CategoryId).ToList();
                    string categoryStatement =
                        " FilteredByCategory (CardId) AS    " +
                        "(SELECT DISTINCT CardId                " +
                        " FROM CardCategory                   " +
                        " WHERE CategoryId IN @categoryIds )    ";
                    if (string.IsNullOrEmpty(withClause))
                    {
                        withClause += "WITH " + categoryStatement;
                    }
                    else
                    {
                        withClause += ", " + categoryStatement;
                    }
                }

                //type subquery
                if (types != null) {
                    fromClause += " JOIN FilteredByType ft ON Card.CardId = ft.CardId ";
                    typeIds = types.Select(x => x.TypeId).ToList();
                    string typeStatement =
                        " FilteredByType (CardId) AS        " +
                        "(SELECT DISTINCT CardId                " +
                        " FROM CardsTypeCard                    " +
                        " WHERE TypeId IN @typeIds )            ";
                    if (string.IsNullOrEmpty(withClause))
                    {
                        withClause += "WITH " + typeStatement;
                    }
                    else
                    {
                        withClause += ", " + typeStatement;
                    }
                }


                //rarity subquery
                if (rarities != null) {
                    fromClause += " JOIN FilteredByRarity fr ON Card.CardId = fr.CardId ";
                    rarityIds = rarities.Select(x => x.RarityId).ToList();
                    string rarityStatement =
                        " FilteredByRarity (CardId) AS  " +
                        "(SELECT DISTINCT CardId            " +
                        " FROM Card                         " +
                        " WHERE RarityId IN @rarityIds )    ";

                    if (string.IsNullOrEmpty(withClause))
                    {
                        withClause += "WITH " + rarityStatement;
                    }
                    else
                    {
                        withClause += ", " + rarityStatement;
                    }
                }
                sqlQuery = withClause + sqlQuery;
                sqlQuery += fromClause;
                sqlQuery += " WHERE Card.GameId = @gameId ";
                //get the filteredCards
                cards = db.Query<Card>(sqlQuery, new { attributeIds = attributeIds, categoryIds = categoryIds, typeIds = typeIds, rarityIds = rarityIds, gameId = gameId}).ToList();

                //build the card with the details
                foreach (Card carda in cards) {
                    //get attributes
                    sqlQuery = " select A.AttributeId, A.Name, A.GameId from Attribute A join CardAttribute CA on A.AttributeId = CA.AttributeId where CA.CardId = @cardId";
                    carda.Attributes = db.Query<Models.Attribute>(sqlQuery, new { cardId = carda.CardId }).ToList();
                    //get categories
                    sqlQuery = " select C.CategoryId, C.Name, C.GameId from Category C join CardCategory CC on C.CategoryId = CC.CategoryId where CC.CardId = @cardId";
                    carda.Categories = db.Query<Category>(sqlQuery, new { cardId = carda.CardId }).ToList();
                    //get types
                    sqlQuery = "select T.TypeId, T.Name, T.GameId from TypeCard T join CardsTypeCard CT on T.TypeId = CT.TypeId where CT.CardId = @cardId";
                    carda.Types = db.Query<TypeCard>(sqlQuery, new { cardId = carda.CardId }).ToList();
                    carda.Image = ConvertImage(carda.Image);
                }
            }

            return cards;
        }

 
        /// <summary>
        ///  Gets the number of copies of each card owned by a user for a single game
        /// </summary>
        /// <param name="userId">Id of the user</param>
        /// <param name="gameId">Id of the game</param>
        /// <returns>The list of cards owned by the user with the respective number of copies</returns>
        public static List<UserCard> GetCopies(int userId, int gameId) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "SELECT * FROM [UserCard] WHERE gameId = @gameId AND userId = @userId";
                List<UserCard> cards = db.Query<UserCard>(sqlQuery, new { userId = userId, gameId = gameId }).ToList();
                return cards;

            }

        }


        public static bool UpdateCollection(List<UserCard> userCards) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "IF EXISTS "
                    + " (SELECT * FROM UserCard WHERE userId = @userId AND cardId = @cardId)    "
                    + "   UPDATE UserCard SET copies = @copies                                  "
                    + "   WHERE userId = @userId AND cardId = @cardId                           "
                    + " ELSE "
                    + "   INSERT INTO UserCard VALUES (@userId, @cardId, @copies, @gameId) ";

                //add the new cards to the collection and update the numbers of the existing ones
                List<int> cardIds = new List<int>();
                try {
                    foreach (UserCard userCard in userCards) {
                        db.Execute(sqlQuery, new { userId = userCard.UserId, cardId = userCard.CardId, copies = userCard.Copies, gameId = userCard.GameId });
                        cardIds.Add(userCard.CardId);
                    }
                } catch (Exception e) {
                    return false;
                }
                //remove the cards that are not in the deck anymore
                try {
                    sqlQuery = "DELETE FROM UserCard WHERE userId = @userId AND NOT (cardId IN @cardIds) ";
                    db.Execute(sqlQuery, new { userId = userCards[0].UserId, cardIds = cardIds });
#pragma warning disable CS0168 // Variable is declared but never used
                } catch (Exception e) {
#pragma warning restore CS0168 // Variable is declared but never used
                    return false;
                }
                return true;
            }
        }


        /// <summary>
        /// Converts an image to base64 and adds the necessary prefix in front of the resulting string
        /// </summary>
        /// <param name="name">Name of the card file in the database</param>
        /// <returns>A string in base64 of the converted image</returns>
        private static string ConvertImage(string name) {
            string basePath = "C:\\BetterProgetto\\Media\\Cards";
            name = Path.Combine(basePath, name);
            byte[] arrayImage = File.ReadAllBytes(name);
            string base64Image = Convert.ToBase64String(arrayImage);
            //byte[] bytes = Convert.FromBase64String(base64Image);
            return "data:image / png; base64," + base64Image;
        }
    }
}