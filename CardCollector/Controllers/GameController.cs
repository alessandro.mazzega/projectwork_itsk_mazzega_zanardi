﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CardCollector.Models;
using CardCollector.Logic;

namespace CardCollector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        // GET: api/Game
        [HttpGet]
        public List<Game> Get()
        {
            return GenericDB.GetFromDb<Game>("Game");
        }

        // GET: api/Game/5
        [HttpGet("{id}")]
        public Game Get(int id)
        {
            return GenericDB.GetFromDb<Game>("Game", id);
        }

        // POST: api/Game
        [HttpPost]
        public IActionResult Post() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // PUT: api/Game/5
        [HttpPut("{id}")]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
