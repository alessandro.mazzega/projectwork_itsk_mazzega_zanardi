﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// Rarity indicates the chance of finding a card in packs. Rarity does not have an effect on gameplay
    /// MTG values: Common, Uncommon, Rare, MythicRare
    /// YuGiOh values: Common, Rare, SuperRare, UltraRare, ...
    /// </summary>
    public class Rarity {
        /// <summary>
        /// 1-Common 2-Uncommon 3-Rare 4-MythicRare
        /// 5-Common 5-Rare 6-SuperRare 7-UltraRare ...
        /// </summary>
        public int RarityId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
