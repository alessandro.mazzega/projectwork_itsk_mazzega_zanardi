import { User } from './user.model';
import { Card } from './card.model';

export class UserCard {
    userId: number;
    cardId: number;
    gameId: number;
    copies: number;
    user: User;
    card: Card;
}