﻿using System;
using System.Collections.Generic;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase {
        // GET: api/User
        [HttpGet]
        public List<User> Get() {
            return UserDB.GetUsers();
        }

        // GET: api/User/5
        [HttpGet("{id}")]
        public User Get(int id) {
            return UserDB.GetUser(id);
        }

        [Route("Login"), HttpPost]
        public IActionResult TryLogin(Models.User user) {
            user = UserDB.TryLogin(user);
            if (user != null) {
                return StatusCode(200, user);
            } else {
                return StatusCode(401, user);
            }
        }

        // PUT: api/User/Signup
        [HttpPut(), Route("Signup")]
        public int Put(User user) {
            try {
                return UserDB.AddUser(user);
            } catch (Exception) {
                return -1;
            }
        }

        [HttpPost, Route("Username")]
        public bool UsernameAvailable(string username) {
            return UserDB.CheckUsername(username);
        }

        [HttpPost, Route("Email")]
        public bool MailAvailable(string email) {
            return UserDB.CheckMail(email);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id) {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
