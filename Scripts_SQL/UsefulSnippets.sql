--controlla se la tabella NomeTabella esiste, se esiste fa x
IF(EXISTS(SELECT * FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME='NomeTabella'))
BEGIN
select *
END


--controlla se la tabella NomeTabella NON esiste, se non esiste fa x
IF NOT(EXISTS(SELECT * FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME='NomeTabella'))
BEGIN
select *
END