﻿using System;
using System.Collections.Generic;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class CardController : ControllerBase {
        // GET: api/Card
        [HttpGet]
        public IActionResult Get() {
            //return CardDB.GetFromDb<Card>("Card");
            List<Card> cards =  CardDB.GetCards();
            return Ok(cards);
        }

        // GET: api/Card/2
        [HttpGet("{id}")]
        public Card GetCard(int id) {
            return CardDB.GetCard(id);
        }

        // GET: api/Card/Game/2
        [HttpGet("Game/{id}")]
        public IActionResult GetCardsByGame(int id) {
            return Ok(CardDB.GetCardsByGame(id));
        }

        [HttpPost("Filter")]
        public List<Card> GetFilteredCards(TempCard filter) {
            return CardDB.FilterCards(filter.Card, filter.Attributes, filter.Categories, filter.Types, filter.Rarities, filter.GameId);
        }

        [HttpPost("Copies")]
        public List<UserCard> GetCopies(TempUserCard userCard) {
            return CardDB.GetCopies(userCard.UserId, userCard.GameId);
        }

        // PUT: api/Card/Collection
        [HttpPut(), Route("Collection")]
        public bool Put([FromBody] List<UserCard> cards) {
            return CardDB.UpdateCollection(cards);
        }


        // PUT: api/Card/5
        [HttpPut]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
    /// <summary>
    /// Model used to read data from the json sent by the web app
    /// </summary>
    public class TempCard {
        public Card Card { get; set; }
        public List<Models.Attribute> Attributes { get; set; }
        public List<Category> Categories { get; set; }
        public List<TypeCard> Types { get; set; }
        public List<Rarity> Rarities { get; set; }
        public int GameId { get; set; }
        public User User { get; set; }
    }

    public class TempUserCard {
        public int UserId { get; set; }
        public int CardId { get; set; }
        public int GameId { get; set; }
    }
}
