import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ApiService, tempCard } from 'src/app/services/services';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { Type } from 'src/app/models/type.model';
import { Attribute } from 'src/app/models/attribute.model';
import { Rarity } from 'src/app/models/rarity.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Card } from 'src/app/models/card.model';
import { UserCard } from 'src/app/models/userCard.model';
import { EditorCard } from '../deck-card/deck-card.component';

@Component({
  selector: 'app-deck-filter',
  templateUrl: 'deck-filter.component.html'
})
export class DeckFilterComponent implements OnInit {
  @Input() listCard: EditorCard[];
  @Output() filterEvent = new EventEmitter<EditorCard[]>();
  gameId;
  attributes: Attribute[];
  categories: Category[];
  types: Type[];
  rarities: Rarity[];

  //form stuff
  form: FormGroup;
  dataLoaded: boolean;

  //multiselect stuff
  attributeOptions: Attribute[];
  categoryOptions: Category[];
  typeOptions: Type[];
  rarityOptions: Rarity[];

  constructor(private srv: ApiService, private router: Router, private builder: FormBuilder) { }

  ngOnInit() {
    this.gameId = localStorage.getItem('game');
    this.dataLoaded = false;
    this.getData();
    this.dataLoaded = true;
    this.form = this.createForm();
  }

  getData() {
    this.srv.GetType().subscribe(res => {
      this.types = res;
      this.typeOptions = [];
      for (let i = 0; i < this.types.length; i++) {
        if (this.types[i].gameId == this.gameId) {
          this.typeOptions.push(this.types[i]);
        }
      }
    });

    this.srv.GetAttributes().subscribe(res => {
      this.attributes = res;
      this.attributeOptions = [];
      for (let i = 0; i < this.attributes.length; i++) {
        if (this.attributes[i].gameId == this.gameId) {
          this.attributeOptions.push(this.attributes[i]);
        }
      }
    });

    this.srv.GetCategories().subscribe(res => {
      this.categories = res;
      this.categoryOptions = [];
      for (let i = 0; i < this.categories.length; i++) {
        if (this.categories[i].gameId == this.gameId) {
          this.categoryOptions.push(this.categories[i]);
        }
      }
    });

    this.srv.GetRarities().subscribe(res => {
      this.rarities = res;
      this.rarityOptions = [];
      for (let i = 0; i < this.rarities.length; i++) {
        if (this.rarities[i].gameId == this.gameId) {
          this.rarityOptions.push(this.rarities[i]);
        }
      }
    });
  }

  createForm() {
    return this.builder.group({
      category: null,
      attribute: null,
      type: null,
      rarity: null
    });
  }

  send() {
    if (this.form.valid) {
      let filteredCards: EditorCard[] = []; //load all the cards before filtering

      let filter = new tempCard();
      if (this.form.controls["type"].value != null) {
        //the value array could be empty, so it's been set to null to avoid confusion
        this.form.controls["type"].value.length > 0 ? filter.types = this.form.controls["type"].value : filter.types = null;
      } else {
        filter.types = this.form.controls["type"].value;
      }

      if (this.form.controls["category"].value != null) {
        //the value array could be empty, so it's been set to null to avoid confusion
        this.form.controls["category"].value.length > 0 ? filter.categories = this.form.controls["category"].value : filter.categories = null;
      } else {
        filter.categories = this.form.controls["category"].value;
      }

      if (this.form.controls["attribute"].value != null) {
        //the value array could be empty, so it's been set to null to avoid confusion
        this.form.controls["attribute"].value.length > 0 ? filter.attributes = this.form.controls["attribute"].value : filter.attributes = null;
      } else {
        filter.attributes = this.form.controls["attribute"].value;
      }

      if (this.form.controls["rarity"].value != null) {
        //the value array could be empty, so it's been set to null to avoid confusion
        this.form.controls["rarity"].value.length > 0 ? filter.rarities = this.form.controls["rarity"].value : filter.rarities = null;
      } else {
        filter.rarities = this.form.controls["rarity"].value;
      }

      filter.gameId = this.gameId;


      //filtering
      console.log(filteredCards);
      this.srv.GetFilteredCards(filter).subscribe(res => {
        res.forEach(card => {
          let index = this.listCard.findIndex(x => x.card.cardId == card.cardId);
          if (index != -1) {
            filteredCards.push(this.listCard[index]);
          }
        });
        this.filterEvent.emit(filteredCards);
      });

      //end

    } else {
      console.log("cuked");
      for (let c in this.form.controls) {
        this.form.controls[c].markAsTouched();
      }
    }
  }

  onReset() {
    this.form.reset();
    let cards: Card[] = [];

    this.filterEvent.emit(this.listCard);
  }
}
