import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './components/user/signup/signup.component';
import { CardComponent } from './components/collection/card/card.component';
import { LoginComponent } from './components/user/login/login.component';
import { CardListComponent } from './components/collection/card-list/card-list.component';
import { DeckListComponent } from './components/deck-list/deck-list.component';
import { FilterComponent } from './components/collection/filter/filter.component';
import { RedirectComponent } from './components/redirect/redirect.component';
import { HomeComponent } from './components/home-page/home-page.component';
import { DeckComponent } from './components/deck-editor/deck/deck.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path:'signup',
    component: SignupComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'collection',
    component: CardListComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path:'decks',
    component: DeckListComponent
  },
  {
    path: 'editor/:deckId',
    component: DeckComponent
  },
  {
    path:'filter',
    component: FilterComponent
  },
  {
    path:'redirect',
    component: RedirectComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
