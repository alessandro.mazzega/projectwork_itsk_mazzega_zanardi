﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class DeckController : ControllerBase {
        // GET: api/Deck
        [HttpGet]
        public List<Deck> Get() {
            return GenericDB.GetFromDb<Deck>("Deck");
        }

        // GET: api/Deck/5
        [HttpGet("{id}")]
        public Deck Get(int id) {
            return DeckDB.GetDeck(id);
        }

        // PUT: api/Deck
        [HttpPut]
        public int Put([FromBody] Deck deck) {
            return DeckDB.AddDeck(deck.Name, deck.GameId, deck.UserId);
        }

        // POST: api/Deck/Decklis/
        [HttpPost, Route("Decklist")]
        public List<DeckCard> Post([FromBody] Deck deck) {
            try {
                return DeckDB.GetDecklist(deck);
            } catch (Exception) {
                throw;
            }
        }

        // PUT: api/Deck/Decklist/5
        [HttpPut, Route("Decklist")]
        public bool Put([FromBody] List<DeckCard> deckList) {
            try {
                return DeckDB.UpdateDecklist(deckList);
            } catch (Exception) {
                return false;
            }
        }

        // GET api/Deck/Export/5
        [HttpGet, Route("Export/{deckId}")]
        public IActionResult Export(int deckId) {
            try {
                return Ok(new { text = DeckDB.ExportDeck(deckId) });
            } catch (Exception) {
                return BadRequest();
            }
        }  

        // PUT: api/Deck/Import/5
        [HttpPut, Route("Import/{userId}")]
        public IActionResult Import(int userId, [FromBody] List<string> deckString) {

            var result = DeckDB.ImportDeck(deckString[0], userId);
            if (result < 1) {
                return BadRequest(result);
            }
            return Ok(result);
        }



        // DELETE: api/Deck/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id) {
            if (DeckDB.RemoveDeck(id)) {
                return StatusCode(200, true);
            } else {
                return StatusCode(500, false);
            }
        }
    }
}
