import { Component, OnInit, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements AfterContentInit {

  constructor(private router: Router) { }

  ngAfterContentInit() {
    setTimeout(() => {
      this.router.navigate(["home"]);
    }, 3000);
  }
}
