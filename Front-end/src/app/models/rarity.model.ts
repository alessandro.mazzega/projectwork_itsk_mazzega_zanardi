export class Rarity {
  rarityId: number;
  name: string;
  gameId: number;
}
