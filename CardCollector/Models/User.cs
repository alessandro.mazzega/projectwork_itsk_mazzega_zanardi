﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {

    public class User {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        /// <summary>
        /// Used to determine what operation the user is allowed to use
        /// Values: 0 = admin, 1 = moderator, 2 = basic user
        /// </summary>
        public int Role { get; set; }
        public string Email { get; set; }
    }
}
