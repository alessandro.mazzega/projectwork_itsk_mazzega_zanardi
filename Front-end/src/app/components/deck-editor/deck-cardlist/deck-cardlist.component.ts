import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';
import { ApiService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { EditorCard } from '../deck-card/deck-card.component';
import { Card } from 'src/app/models/card.model';

@Component({
  selector: 'app-deck-cardlist',
  templateUrl: './deck-cardlist.component.html',
  styleUrls: []
})
export class DeckCardlistComponent implements OnInit {
  @Input() listCard: EditorCard[];
  @Output() saveEvent = new EventEmitter<boolean>();
  shownCards: EditorCard[];
  detail: number;
  game: number;
  showFilter: boolean;
  showGame: boolean;

  constructor(private srv: ApiService, private router: Router) {

  }

  ngOnInit() {
    this.detail = -1;
    this.shownCards = this.listCard;
    console.log(this.shownCards);
    this.showFilter = false;
  }

  onToggleFilter() {
    this.showFilter == true ? this.showFilter = false : this.showFilter = true;
  }

  filterCards($event: EditorCard[]) {
    this.shownCards = $event;
    this.shownCards.forEach(editorC => {
      this.listCard[this.listCard.findIndex(x => x.card.cardId == editorC.card.cardId)].copiesInDeck = editorC.copiesInDeck;
    });
  }

  toggleDetail($event: number){
    this.detail = $event; 
  }
  updateCard($event: EditorCard) {
    this.listCard[this.listCard.findIndex(x => x.card.cardId == $event.card.cardId)].copiesInDeck = $event.copiesInDeck;
  }

  save(){
    this.saveEvent.emit();
  }
}