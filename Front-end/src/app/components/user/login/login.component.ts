import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})

export class LoginComponent implements OnInit {

  form: FormGroup;
  submitted: boolean;

  constructor(private userSrv: UserService, private router: Router, private builder: FormBuilder) {

  }
  ngOnInit() {

    this.submitted = false;
    this.form = this.createForm();
  }

  createForm() {
    return this.builder.group({
      username: null,
      password: null
    });
  }

  send() {
    this.submitted = true;
    if (this.form.valid) {
      let tempUser: User = new User;
      tempUser.username = this.form.controls["username"].value;
      tempUser.password = this.form.controls["password"].value;
      this.submitted = false;
      let user: User = new User;
      user.username = tempUser.username;
      user.password = tempUser.password;
      this.userSrv.TryLogin(user).subscribe(res => {
        if (res != null) {//login successful
          //go to home
          localStorage.setItem("user", JSON.stringify(res.userId));
          console.log(res.username);
          localStorage.setItem("username", JSON.stringify(res.username));
          this.router.navigate(["home"]);
        } else {
          //retry login, maybe with error message
          console.log('wrong username/password combination');
        }
      });
    } else { //the form was invalid, the user needs to check the inserted data
      console.log("the form was not ready to be submitted");
      for (let c in this.form.controls) {
        this.form.controls[c].markAsTouched();
      }
    }
  }

}
