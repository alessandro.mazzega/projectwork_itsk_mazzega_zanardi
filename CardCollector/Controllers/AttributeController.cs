﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        // GET: api/Attribute
        [HttpGet]
        public List<Models.Attribute> Get() {
            return GenericDB.GetFromDb<Models.Attribute>("Attribute");
        }

        // GET: api/Attribute/5
        [HttpGet("{id}")]
        public Models.Attribute Get(int id) {
            return GenericDB.GetFromDb<Models.Attribute>("Attribute", id);
        }

        // POST: api/Attribute
        [HttpPost]
        public IActionResult Post() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // PUT: api/Attribute/5
        [HttpPut("{id}")]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
