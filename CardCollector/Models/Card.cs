﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// This class describes a generic card of a TCG. Each field has to be interpreted accordingly to its TCG 
    /// </summary>
    public class Card {
        
        public int CardId { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public string Name { get; set; }
        public string PaperId { get; set; }
        public int RarityId { get; set; }
        /// <summary>
        /// location of the card image on the server's storage
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Cost needed to play the card in a match of a certain game
        /// </summary>
        public string Cost { get; set; }
        public Rarity Rarity { get; set; }
        public List<Attribute> Attributes { get; set; }
        public List<Category> Categories { get; set; }
        public List<TypeCard> Types { get; set; }

        public string GetCostIdentity() {
            Regex rgx = new Regex("[^a-zA-Z]");
            return rgx.Replace(this.Cost, "");
        }
    }
}
