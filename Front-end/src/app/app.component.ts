import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeckService, ApiService, UserService } from './services/services';
import { Deck } from './models/deck.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loggedUserName: string
  title = 'CardCollector';
  deck: Deck;
  dataLoaded: boolean;

  constructor(private router: Router, private userSrv: UserService) {

  }

  ngOnInit() {
    this.loggedUserName = "";
    this.dataLoaded = false;
    //only for demo purposes
    if (!this.userLoggedIn()) {
      localStorage.setItem('game', '1');
    }

  }

  userLoggedIn() {
    if (localStorage.getItem("user") !== null) {
      this.loggedUserName = (localStorage.getItem("username")).replace('"', '').replace('"', '');
      return true;
    } else {
      return false;
    }
  }

  logout() {
    localStorage.removeItem("user");
    localStorage.removeItem("username");
    this.router.navigate(["redirect"]);
  }
}
