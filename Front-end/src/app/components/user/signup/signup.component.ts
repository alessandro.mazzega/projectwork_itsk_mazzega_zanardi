import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { UsernameAvailable } from './confirm-username.validator';
@Component({
  selector: 'app-signup',
  templateUrl: 'signup.component.html',
  styleUrls: ['signup.component.css']
})

export class SignupComponent implements OnInit {

  form: FormGroup;
  submitted: boolean;
  error: boolean;
  constructor(private userSrv: UserService, private router: Router, private builder: FormBuilder) {

  }

  ngOnInit() {
    this.submitted = false;
    this.error = false;
    this.form = this.createForm();
    console.log(this.form);
  }

  createForm() {
    return this.builder.group({
      username: ["", Validators.compose([
        Validators.required
      ])],
      email: ["", Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ["", Validators.compose([
        Validators.required
      ])],
      confirmPassword: ["", Validators.compose([
        Validators.required
      ])]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }

  send() {
    this.submitted = true;
    this.error = false;
    if (this.form.valid) {
      let tempUser: TempUser = new TempUser;
      tempUser.username = this.form.controls["username"].value;
      tempUser.email = this.form.controls["email"].value;
      tempUser.password = this.form.controls["password"].value;
      tempUser.confirmPassword = this.form.controls["confirmPassword"].value;
      this.submitted = false;
      let user: User = new User;
      user.username = tempUser.username;
      user.email = tempUser.email;
      user.password = tempUser.password;
      user.role = 2;
      user.userId = 0;
      this.userSrv.Signup(user).subscribe(res => {
        if (res != -1) {//signup successful
          //go to home, but now logged in
          localStorage.setItem("user", JSON.stringify(res));
          localStorage.setItem("username", JSON.stringify(user.username));
          this.router.navigate(["redirect"]);
        } else {
          this.error = true;
          //retry signup, maybe with error message
          console.log("Could have been worse");
        }
      });
    } else { //the form was invalid, the user needs to check the inserted data
      this.form.markAllAsTouched();
      return;
    }
  }
}

class TempUser {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
}



