﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// This class describes the attributes of cards. Attributes do have an effect on gameplay
    /// MTG values: Basic, Legendary, Snow, World
    /// YuGiOh values: Fire, Water, ..., Equipment, Quick, ...
    /// </summary>
    public class Attribute {
        public int AttributeId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
