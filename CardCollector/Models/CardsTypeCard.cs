﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// Class used to represent a N:N relation between the entities Card and TypeCard
    /// </summary>
    public class CardsTypeCard {
        public int CardsTypeCardId { get; set; }
        public int CardId { get; set; }
        public int TypeCardId { get; set; }
        public Card Card { get; set; }
        public TypeCard TypeCard { get; set; }

        public CardsTypeCard(int typeId, int cardId) {
            this.CardId = cardId;
            this.TypeCardId = typeId;
        }
    }
}
