-- script to show a card with all its components
SELECT c.[Name], a.Name, ct.Name, t.Name
  FROM [Card] c join CardAttribute ca on c.CardId = ca.CardId
   join CardCategory cc on cc.CardId = c.CardId
   join Attribute a on ca.AttributeId = a.AttributeId
   join Category ct on ct.CategoryId = cc.CategoryId
   left join CardsTypeCard ctc on ctc.CardId = c.CardId
   left join TypeCard t on t.TypeId = ctc.TypeId