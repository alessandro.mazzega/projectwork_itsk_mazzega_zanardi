﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// This class describes the category of cards. Categories do have an effect on gameplay
    /// MTG values: Creature, NonCreature, Land
    /// YuGiOh values: Monster, Spell, Trap
    /// </summary>
    public class Category {
        /// <summary>
        /// 1-Creature 2-NonCreature 3-Land 4-Monster 5-Spell 6-Trap
        /// </summary>
        public int CategoryId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
