import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './components/user/signup/signup.component';
import { CardComponent } from './components/collection/card/card.component';
import { CardListComponent } from './components/collection/card-list/card-list.component';
import { LoginComponent } from './components/user/login/login.component';
import { DeckListComponent } from './components/deck-list/deck-list.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { FilterComponent } from './components/collection/filter/filter.component';
import { DeckComponent } from './components/deck-editor/deck/deck.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RedirectComponent } from './components/redirect/redirect.component';
import { UserService, ApiService, DeckService } from './services/services';
import { ChangeGameComponent } from './components/collection/change-game/change-game.component';
import { HomeComponent } from './components/home-page/home-page.component';
import { CleanCostPipe } from './pipes/clean.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { NewDeckComponent } from './components/new-deck/new-deck.component';
import { DeckCardlistComponent } from './components/deck-editor/deck-cardlist/deck-cardlist.component';
import { DeckCardComponent } from './components/deck-editor/deck-card/deck-card.component';
import { DeckFilterComponent } from './components/deck-editor/deck-filter/deck-filter.component';

//kendo stuff
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { PopupModule } from '@progress/kendo-angular-popup';
import { DialogsModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    CardComponent,
    CardListComponent,
    LoginComponent,
    DeckListComponent,
    SafeHtmlPipe,
    FilterComponent,
    DeckComponent,
    RedirectComponent,
    ChangeGameComponent,
    NewDeckComponent,
    DeckCardlistComponent,
    DeckCardComponent,
    DeckFilterComponent,
    HomeComponent,
    CleanCostPipe,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DropDownsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    PopupModule,
    NgbModule,
    DialogsModule,
    ButtonsModule,
  ],
  providers: [UserService, ApiService, DeckService],
  bootstrap: [AppComponent]
})
export class AppModule { }
