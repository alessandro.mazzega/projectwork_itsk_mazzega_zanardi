import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import { style, state, animate, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-deck-card',
  templateUrl: 'deck-card.component.html',
  styles: [],

  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(300, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(150, style({ opacity: 0 }))
      ])
    ])
  ]

})
export class DeckCardComponent implements OnInit {

  @Input() editorCard: EditorCard;
  @Output() updateEvent = new EventEmitter<EditorCard>();
  @Input() showDetail: boolean;
  @Output() detailEvent = new EventEmitter<number>();

  constructor() {

  }

  ngOnInit() {
    console.log(this.editorCard);
  }

  add() {
    this.editorCard.copiesInDeck++;
    this.updateEvent.emit(this.editorCard);
  }

  sub() {
    if (this.editorCard.copiesInDeck > 0)
      this.editorCard.copiesInDeck--;
    this.updateEvent.emit(this.editorCard);
  }

  toggleDetail() {
    console.log(this.showDetail)
    if (this.showDetail) { //unfocus card
      this.detailEvent.emit(-1);
    } else { //focus card
      this.detailEvent.emit(this.editorCard.card.cardId);
    }
  }
}

//card that shows the copies in the deck
export class EditorCard {
  card: Card;
  copiesInDeck: number;
}
