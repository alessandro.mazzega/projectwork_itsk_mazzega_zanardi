﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    ///<summary>
    ///Class used to represent a N:N relation between the entities User and Cards. The relation identifies a card owned by a certain user.
    ///</summary>
    public class UserCard {
    public int UserCardId { get; set; }
        public int UserId { get; set; }
        public int CardId { get; set; }
        public int GameId { get; set; }
        public int Copies { get; set; }
        public User User { get; set; }
        public Card Card { get; set; }
    }
}
