import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService, DeckService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Deck } from 'src/app/models/deck.model';
import { Game } from 'src/app/models/game.model';

@Component({
  selector: 'app-newdeck',
  templateUrl: 'new-deck.component.html',
  styles: ['new-deck.component.css']
})
export class NewDeckComponent implements OnInit {


  @Output() deckEvent = new EventEmitter<Deck>();
  form: FormGroup;
  dataLoaded: boolean;
  decklist: Deck[];
  games: Game[];

  constructor(private srv: ApiService, private router: Router, private builder: FormBuilder, private deckSrv: DeckService) { }


  ngOnInit() {
    this.dataLoaded = false;
    this.getData();
    this.dataLoaded = true;
    this.form = this.createForm()
  }

  getData() {
    this.srv.GetGames().subscribe(res => {
      this.games = res;
      console.log(this.games)
    })
  }

  createForm() {
    console.log('Im uploading the form')
    return this.builder.group({
      name: null,
      game: null
    })
  };


  send() {
    if (this.form.valid) {
      let deck: Deck = {
        gameId: this.form.controls["game"].value.gameId,
        deckId: -1,
        cardCount: 0,
        userId: +localStorage.getItem('user'),
        name: this.form.controls["name"].value,
        deckList: [],
        game: this.form.controls["game"].value,
      };
      console.log(deck);
      this.deckSrv.CreateDeck(deck).subscribe(res => {
        if (res != -1){
          this.editDeck(res);
        }
      });

    }
  }

  editDeck(deckId: number){
    this.router.navigate(["editor", deckId]);
  }
}
