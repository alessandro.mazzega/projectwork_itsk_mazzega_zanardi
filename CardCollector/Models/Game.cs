﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// Used as a base for the other objects.
    /// The only implented games are: 1-MTG 2-YuGiOh
    /// </summary>
    public class Game {

        public int GameId { get; set; }
        public string Name { get; set; }
    }
}
