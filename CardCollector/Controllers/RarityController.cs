﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RarityController : ControllerBase
    {
        // GET: api/Rarity
        [HttpGet]
        public List<Rarity> Get() {
            return GenericDB.GetFromDb<Rarity>("Rarity");
        }

        // GET: api/Rarity/5
        [HttpGet("{id}")]
        public Rarity Get(int id) {
            return GenericDB.GetFromDb<Rarity>("Rarity", id);
        }

        // POST: api/Rarity
        [HttpPost]
        public IActionResult Post() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // PUT: api/Rarity/5
        [HttpPut("{id}")]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
