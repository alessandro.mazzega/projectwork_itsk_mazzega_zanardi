﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    /// <summary>
    /// Types do have an effect on gameplay.
    /// MTG values: Merfolk, Dragon, ..., Instant, Planeswalker, ...
    /// YuGiOh values: Warrior, Fairy, ...
    /// </summary>
    public class TypeCard {
        public int TypeId { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Value: 1 = MTG, 2 = YuGiOh
        /// </summary>
        public int GameId { get; set; }
        public Game Game { get; set; }
    }
}
