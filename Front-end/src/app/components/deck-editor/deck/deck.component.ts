import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Deck } from 'src/app/models/deck.model';
import { DeckCard } from 'src/app/models/deckCard.model';
import { Router, ActivatedRoute } from '@angular/router';
import { DeckService, ApiService } from 'src/app/services/services';
import { EditorCard } from '../deck-card/deck-card.component';
import { UserCard } from 'src/app/models/userCard.model';

@Component({
  selector: 'app-deck',
  templateUrl: 'deck.component.html',
  styles: ['deck.component.css']
})
export class DeckComponent implements OnInit {
  deck: Deck;
  deckId: number;
  deckList: DeckCard[];
  tempDeckList: EditorCard[];
  listCard: EditorCard[];
  dataLoaded: boolean;
  constructor(private router: Router, private deckSrv: DeckService, private apiSrv: ApiService, private activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.deckId = +this.activatedRoute.snapshot.paramMap.get("deckId");
    this.listCard = [];
    this.dataLoaded = false;
    await this.getData();
    await this.updateList();
    this.dataLoaded = true;
  }

  async getData() {
    this.deck = await this.deckSrv.GetDeck(this.deckId).toPromise();
    localStorage.setItem("game", this.deck.gameId.toString())
    this.deckList = await this.deckSrv.GetDeckList(this.deck).toPromise();

    this.deckList.sort((a, b) => { return a.cardId - b.cardId })
    this.loadCards();
  }

  //load the cards with the copies (in collection and deck)
  loadCards() {
    let userId: number = +localStorage.getItem("user");
    //get the whole list of cards
    this.apiSrv.GetCards().subscribe(cards => {
      //convert them into EditorCards to register the copies in the deck
      cards.forEach(card => {
        card.copies = 0;
        let editorCard: EditorCard = {
          card: card,
          copiesInDeck: 0
        }
        this.listCard.push(editorCard);
      });
      //sort the list for a faster search
      this.listCard.sort((a, b) => a.card.cardId - b.card.cardId);
      //add the copies owned by the user
      if (userId != null) {
        let userCard: UserCard = {
          gameId: +localStorage.getItem("game"),
          userId: userId,
          cardId: -1,
          copies: null,
          user: null,
          card: null
        };
        this.apiSrv.GetCopies(userCard).subscribe(userCards => {
          //sort the list for a faster search
          userCards.sort((a, b) => { return a.cardId - b.cardId });
          userCards.forEach(userC => {
            let completed: boolean = false;
            let index: number = 0;
            //search for the card in the collection
            while (!completed) {
              if (userC.cardId == this.listCard[index].card.cardId) {
                //the card is in the user collection, search over
                this.listCard[index].card.copies = userC.copies;
                completed = true;
              } else if (userC.cardId > this.listCard[index].card.cardId) {
                //the cards was not found, search over
                completed = true;
              } else {
                //searching...
                index++;
              }
            }
          });

          //add the copies in the deck
          this.deckSrv.GetDeckList(this.deck).subscribe(res => {
            this.deckList = res;
            this.deckList.forEach(deckCard => {
              let completed: boolean = false;
              let index: number = 0;
              while (!completed) {
                if (deckCard.cardId == this.listCard[index].card.cardId) {
                  //the card is in the user collection, search over
                  this.listCard[index].copiesInDeck = deckCard.copies;
                  completed = true;
                } else if (deckCard.cardId < this.listCard[index].card.cardId) {
                  //the cards was not found, search over
                  completed = true;
                } else {
                  //searching...
                  index++;
                }
              }
            });
          })
        });
      }
    });
  }

  ngDoCheck() {
    this.updateList();
  }

  async updateList() {
    this.tempDeckList = [];
    this.listCard.forEach(editorC => {
      if (editorC.copiesInDeck > 0) {
        this.tempDeckList.push(editorC);
      }
    });

    this.tempDeckList.forEach(editorC => {
      if (editorC.copiesInDeck <= 0){
        this.tempDeckList.splice(this.tempDeckList.indexOf(editorC));
      }
    });
  }

  //sends the deck to the db
  save() {
    this.deckList = [];
    //add the new cards to the deck
    this.tempDeckList.forEach(editorCard => {
      //add a new deckCard
      let deckCard: DeckCard = {
        deckCardId: -1,
        cardId: editorCard.card.cardId,
        copies: editorCard.copiesInDeck,
        gameId: editorCard.card.gameId,
        deckId: this.deck.deckId,
        card: null,
        game: null,
        deck: null
      };
      this.deckList.push(deckCard);
    });
    //remove the cards that are not in the deck anymore
    for (let i = this.deckList.length - 1; i >= 0; i--) {
      let deckCard = this.deckList[i];
      if (deckCard.copies <= 0) {
        this.deckList.splice(i);
      }
    }

    //send new deckList to the api
    this.deckSrv.SendDeckList(this.deckList).subscribe(res => {
      if (res) {//the deck was sumbmitted successfully
        this.router.navigate(["decks"]);
      } else {
        console.log("wack");
      }
    })
  }
}