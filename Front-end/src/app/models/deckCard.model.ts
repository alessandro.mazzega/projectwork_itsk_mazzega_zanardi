import { Card } from './card.model';
import { Deck } from './deck.model';
import { Game } from './game.model';

export class DeckCard {
    deckCardId: number;
    cardId: number;
    copies: number;
    gameId: number;
    deckId: number;
    card: Card;
    deck: Deck;
    game: Game;
}