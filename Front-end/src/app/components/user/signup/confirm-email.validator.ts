import { FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/services';

// custom validator to check that two fields match
export function EmailAvailable() {
    return async (formGroup: FormGroup) => {
        const control = formGroup.controls["email"];
		let userSrv: UserService;
		let emailTaken = await userSrv.EmailAvailable(control.value).toPromise();
        // set error on matchingControl if validation fails
        if (emailTaken) {
            control.setErrors({ emailTaken: true });
        } else {
            control.setErrors(null);
        }
    }
}

