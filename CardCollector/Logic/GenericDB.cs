﻿using CardCollector.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Logic {
    public class GenericDB {
        /// <summary>
        /// Fetch a complete list of items of type T from the Db.
        /// </summary>
        /// <typeparam name="T"> Indicates the type of the items to be fetched</typeparam>
        /// <param name="typeQuery"> Type of the items to be fetched. Must be singular</param>
        /// <returns>A list of objects of type T. Null if the object is not present in the db</returns>
        public static List<T> GetFromDb<T>(string typeQuery) {
            using (SqlConnection db = new SqlConnection("Server=LOCALHOST\\SQLEXPRESS;Database=CardCollector;Integrated Security=True;User Id=CC_Adm; Password = Password!")) {
                string sqlQuery = "Select * From " + typeQuery;
                return db.Query<T>(sqlQuery).ToList();
            }
        }

        /// <summary>
        /// Fetch a single item of type T from the Db
        /// </summary>
        /// <typeparam name="T">Indicates the type of the object to be fetched</typeparam>
        /// <param name="typeQuery">Type of the item to be fetched. Must be singular</param>
        /// <param name="id"></param>
        /// <returns>An object of type T with a particular Id or NULL if not found</returns>
        public static T GetFromDb<T>(string typeQuery, int id) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "Select * From " + typeQuery + " Where GameId = " + id;
                return db.Query<T>(sqlQuery).FirstOrDefault();
            }
        }

        public static Game GetGame(int id) {
            using (SqlConnection db = new SqlConnection("Server = LOCALHOST\\SQLEXPRESS; Database = CardCollector; Integrated Security = True; User Id = CC_Adm; Password = Password!")) {
                string sqlQuery = "SELECT * FROM [Game] WHERE gameId = @gameId";
                return db.Query<Game>(sqlQuery, new { gameId = id }).FirstOrDefault();
            }
        }
    }
}
