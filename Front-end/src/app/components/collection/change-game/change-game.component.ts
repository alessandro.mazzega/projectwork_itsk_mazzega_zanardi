import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/services/services';
import { Router } from '@angular/router';
import { Game } from 'src/app/models/game.model';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-change-game',
  templateUrl: './change-game.component.html',
  styleUrls: ['./change-game.component.css']
})
export class ChangeGameComponent implements OnInit {

  @Output() gameEvent = new EventEmitter<number>();
  games: Game[];
  //form stuff
  form: FormGroup;
  dataLoaded: boolean;
  constructor(private srv: ApiService, private router: Router, private builder: FormBuilder) { }

  ngOnInit() {
    this.dataLoaded = false;
    this.getData();
    this.dataLoaded = true;
    this.form = this.createForm();
  }

  getData() {
    this.srv.GetGames().subscribe(res => {
      this.games = res;
    });
  }

  createForm() {
    return this.builder.group({
      game: localStorage.getItem("game")
    });
  }

  send() {
    if (this.form.valid) {
      this.gameEvent.emit(this.form.controls["game"].value);
    } else {
      console.log("cuked");
      for (let c in this.form.controls) {
        this.form.controls[c].markAsTouched();
      }
    }
  }
}
