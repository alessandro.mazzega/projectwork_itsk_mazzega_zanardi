import { DeckCard } from './deckCard.model';
import { Game } from './game.model';

export class Deck {
  deckId: number;
  name: string;
  gameId: number;
  cardCount: number;
  userId: number;
  deckList: DeckCard[];
  game: Game;
}
