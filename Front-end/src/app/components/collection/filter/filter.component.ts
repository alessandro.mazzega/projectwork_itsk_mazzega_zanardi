import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ApiService, tempCard } from 'src/app/services/services';
import { Router } from '@angular/router';
import { Category } from 'src/app/models/category.model';
import { Type } from 'src/app/models/type.model';
import { Attribute } from 'src/app/models/attribute.model';
import { Rarity } from 'src/app/models/rarity.model';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Card } from 'src/app/models/card.model';
import { UserCard } from 'src/app/models/userCard.model';

@Component({
  selector: 'app-filter',
  templateUrl: 'filter.component.html'
})
export class FilterComponent implements OnInit {
  @Input() cardList: Card[];
  @Output() filterEvent = new EventEmitter<Card[]>();
  gameId;
  attributes: Attribute[];
  categories: Category[];
  types: Type[];
  rarities: Rarity[];

  //form stuff
  form: FormGroup;
  dataLoaded: boolean;

  //multiselect stuff
  attributeOptions: Attribute[];
  categoryOptions: Category[];
  typeOptions: Type[];
  rarityOptions: Rarity[];

  constructor(private srv: ApiService, private router: Router, private builder: FormBuilder) { }

  ngOnInit() {
    this.gameId = localStorage.getItem('game');
    this.dataLoaded = false;
    this.getData();
    this.dataLoaded = true;
    this.form = this.createForm();
    this.cardList.sort((a, b) => { return a.cardId - b.cardId; });
  }

  getData() {
    this.srv.GetType().subscribe(res => {
      this.types = res;
      this.typeOptions = [];
      for (let i = 0; i < this.types.length; i++) {
        if (this.types[i].gameId == this.gameId) {
          this.typeOptions.push(this.types[i]);
        }
      }
    });

    this.srv.GetAttributes().subscribe(res => {
      this.attributes = res;
      this.attributeOptions = [];
      for (let i = 0; i < this.attributes.length; i++) {
        if (this.attributes[i].gameId == this.gameId) {
          this.attributeOptions.push(this.attributes[i]);
        }
      }
    });

    this.srv.GetCategories().subscribe(res => {
      this.categories = res;
      this.categoryOptions = [];
      for (let i = 0; i < this.categories.length; i++) {
        if (this.categories[i].gameId == this.gameId) {
          this.categoryOptions.push(this.categories[i]);
        }
      }
    });

    this.srv.GetRarities().subscribe(res => {
      this.rarities = res;
      this.rarityOptions = [];
      for (let i = 0; i < this.rarities.length; i++) {
        if (this.rarities[i].gameId == this.gameId) {
          this.rarityOptions.push(this.rarities[i]);
        }
      }
    });
  }

  createForm() {
    return this.builder.group({
      category: null,
      attribute: null,
      type: null,
      rarity: null,
      notOwned: true
    });
  }

  send() {
    let filteredCards: Card[] = [];
    if (this.form.valid) {
      //need to use the data from the form to call the api
      let filter = new tempCard();
      if (this.form.controls["type"].value != null) {
        this.form.controls["type"].value.length > 0 ? filter.types = this.form.controls["type"].value : filter.types = null;
      } else {
        filter.types = this.form.controls["type"].value;
      }

      if (this.form.controls["category"].value != null) {
        this.form.controls["category"].value.length > 0 ? filter.categories = this.form.controls["category"].value : filter.categories = null;
      } else {
        filter.categories = this.form.controls["category"].value;
      }

      if (this.form.controls["attribute"].value != null) {
        this.form.controls["attribute"].value.length > 0 ? filter.attributes = this.form.controls["attribute"].value : filter.attributes = null;
      } else {
        filter.attributes = this.form.controls["attribute"].value;
      }

      if (this.form.controls["rarity"].value != null) {
        this.form.controls["rarity"].value.length > 0 ? filter.rarities = this.form.controls["rarity"].value : filter.rarities = null;
      } else {
        filter.rarities = this.form.controls["rarity"].value;
      }

      //add user properties
      filter.gameId = this.gameId;

      //filtering
      this.srv.GetFilteredCards(filter).subscribe(res => {
        //sort for an easier search
        res.sort((a, b) => { return a.cardId - b.cardId; });
        res.forEach(card => {
          let completed: boolean = false;
          let index: number = 0;
          while (!completed) {
            if (card.cardId == this.cardList[index].cardId) {
              //the card is in the user collection, search over
              filteredCards.push(this.cardList[index]);
              completed = true;
            } else if (card.cardId < this.cardList[index].cardId) {
              //the card was not found, search over
              completed = true;
            } else {
              //searching...
              index++;
            }
          }
        });
        if(!this.form.controls["notOwned"].value){
          //remove the cards not collected
          filteredCards = filteredCards.filter(x => x.copies > 0);
        }          
        this.filterEvent.emit(filteredCards);
      });
      //end

    } else {
      console.log("cuked");
      for (let c in this.form.controls) {
        this.form.controls[c].markAsTouched();
      }
    }
  }

  onReset() {
    this.form.reset();
    let cards: Card[] = [];

    this.filterEvent.emit(this.cardList);  }

  //adds the copies owned by the user to a list of cards
  getCardsWithCopies(cards: Card[]): Card[] {
    let result: Card[] = [];
    let userId: number = +localStorage.getItem("user");
    if (userId != null) {
      let userCard: UserCard = {
        gameId: +localStorage.getItem("game"),
        userId: userId,
        cardId: -1,
        copies: null,
        user: null,
        card: null
      };

      this.srv.GetCopies(userCard).subscribe(userCards => {
        cards.forEach(card => {
          userCards.forEach(userC => {
            userC.cardId == card.cardId ? card.copies = userC.copies : card.copies = 0;
          });
          let userCard: UserCard = userCards.find(x => card.cardId == x.cardId);
          userCard == null ? card.copies = 0 : card.copies = userCard.copies;
        });
      });
      return cards;
    }
    return cards;
  }
}
