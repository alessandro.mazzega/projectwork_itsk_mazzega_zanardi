--Insert into Game
INSERT INTO Game VALUES (1, 'Magic: The Gathering')
INSERT INTO Game VALUES (2, 'Yu-Gi-Oh!')

--Insert into Attribute
INSERT INTO Attribute VALUES (1,'Basic', 1)
INSERT INTO Attribute VALUES (2,'Legendary', 1)
INSERT INTO Attribute VALUES (3,'Snow', 1)
INSERT INTO Attribute VALUES (4,'World', 1)
INSERT INTO Attribute VALUES (5,'Fire', 2)
INSERT INTO Attribute VALUES (6,'Water', 2)
INSERT INTO Attribute VALUES (7,'Wind', 2)
INSERT INTO Attribute VALUES (8,'Earth', 2)
INSERT INTO Attribute VALUES (9,'Light', 2)
INSERT INTO Attribute VALUES (10,'Dark', 2)
INSERT INTO Attribute VALUES (11,'NormalSpell', 2)
INSERT INTO Attribute VALUES (12,'QuickPlay', 2)
INSERT INTO Attribute VALUES (13,'Ritual', 2)
INSERT INTO Attribute VALUES (14,'Equipment', 2)
INSERT INTO Attribute VALUES (15,'Continuous', 2)
INSERT INTO Attribute VALUES (16,'Field', 2)
INSERT INTO Attribute VALUES (17,'NormalTrap', 2)
INSERT INTO Attribute VALUES (18,'Counter', 2)
INSERT INTO Attribute VALUES (19,'ContinuousTrap', 2)

--Insert into Category
INSERT INTO Category VALUES (1,'Creature',1)
INSERT INTO Category VALUES (2,'NonCreature',1)
INSERT INTO Category VALUES (3,'Land',1)
INSERT INTO Category VALUES (4,'Instant', 1)
INSERT INTO Category VALUES (5,'Sorcery',1)
INSERT INTO Category VALUES (6,'Enchantment',1)
INSERT INTO Category VALUES (7,'Planeswalker',1)
INSERT INTO Category VALUES (8,'Monster',2)
INSERT INTO Category VALUES (9,'Spell',2)
INSERT INTO Category VALUES (10,'Trap',2)

--Insert into Rarity
INSERT INTO Rarity VALUES (1,'Common',1)
INSERT INTO Rarity VALUES (2,'Uncommon',1)
INSERT INTO Rarity VALUES (3,'Rare',1)
INSERT INTO Rarity VALUES (4,'Mythic',1)
INSERT INTO Rarity VALUES (5,'Common',2)
INSERT INTO Rarity VALUES (6,'Rare',2)
INSERT INTO Rarity VALUES (7,'SuperRare',2)
INSERT INTO Rarity VALUES (8,'UltraRare',2)

--Insert into TypeCard
INSERT INTO TypeCard VALUES (1,'Advisor',1)
INSERT INTO TypeCard VALUES (2,'Aetherborn',1)
INSERT INTO TypeCard VALUES (3,'Ally',1)
INSERT INTO TypeCard VALUES (4,'Angel',1)
INSERT INTO TypeCard VALUES (5,'Antelope',1)
INSERT INTO TypeCard VALUES (6,'Ape',1)
INSERT INTO TypeCard VALUES (7,'Archer',1)
INSERT INTO TypeCard VALUES (8,'Archon',1)
INSERT INTO TypeCard VALUES (9,'Assassin',1)
INSERT INTO TypeCard VALUES (10,'Assembly-Worker',1)
INSERT INTO TypeCard VALUES (11,'Atog',1)
INSERT INTO TypeCard VALUES (12,'Aurochs',1)
INSERT INTO TypeCard VALUES (13,'Avatar',1)
INSERT INTO TypeCard VALUES (14,'Badger',1)
INSERT INTO TypeCard VALUES (15,'Basilisk',1)
INSERT INTO TypeCard VALUES (16,'Bat',1)
INSERT INTO TypeCard VALUES (17,'Bear',1)
INSERT INTO TypeCard VALUES (18,'Beast',1)
INSERT INTO TypeCard VALUES (19,'Beeble',1)
INSERT INTO TypeCard VALUES (20,'Berserker',1)
INSERT INTO TypeCard VALUES (21,'Bird',1)
INSERT INTO TypeCard VALUES (22,'Blinkmoth',1)
INSERT INTO TypeCard VALUES (23,'Boar',1)
INSERT INTO TypeCard VALUES (24,'Bringer',1)
INSERT INTO TypeCard VALUES (25,'Brushwagg',1)
INSERT INTO TypeCard VALUES (26,'Camarid',1)
INSERT INTO TypeCard VALUES (27,'Camel',1)
INSERT INTO TypeCard VALUES (28,'Caribou',1)
INSERT INTO TypeCard VALUES (29,'Carrier',1)
INSERT INTO TypeCard VALUES (30,'Cat',1)
INSERT INTO TypeCard VALUES (31,'Centaur',1)
INSERT INTO TypeCard VALUES (32,'Cephalid',1)
INSERT INTO TypeCard VALUES (33,'Chimera',1)
INSERT INTO TypeCard VALUES (34,'Citizen',1)
INSERT INTO TypeCard VALUES (35,'Cleric',1)
INSERT INTO TypeCard VALUES (36,'Cockatrice',1)
INSERT INTO TypeCard VALUES (37,'Construct',1)
INSERT INTO TypeCard VALUES (38,'Coward',1)
INSERT INTO TypeCard VALUES (39,'Crab',1)
INSERT INTO TypeCard VALUES (40,'Crocodile',1)
INSERT INTO TypeCard VALUES (41,'Cyclops',1)
INSERT INTO TypeCard VALUES (42,'Dauthi',1)
INSERT INTO TypeCard VALUES (43,'Demon',1)
INSERT INTO TypeCard VALUES (44,'Deserter',1)
INSERT INTO TypeCard VALUES (45,'Devil',1)
INSERT INTO TypeCard VALUES (46,'Dinosaur',1)
INSERT INTO TypeCard VALUES (47,'Djinn',1)
INSERT INTO TypeCard VALUES (48,'Dragon',1)
INSERT INTO TypeCard VALUES (49,'Drake',1)
INSERT INTO TypeCard VALUES (50,'Dreadnought',1)
INSERT INTO TypeCard VALUES (51,'Drone',1)
INSERT INTO TypeCard VALUES (52,'Druid',1)
INSERT INTO TypeCard VALUES (53,'Dryad',1)
INSERT INTO TypeCard VALUES (54,'Dwarf',1)
INSERT INTO TypeCard VALUES (55,'Efreet',1)
INSERT INTO TypeCard VALUES (56,'Elder',1)
INSERT INTO TypeCard VALUES (57,'Eldrazi',1)
INSERT INTO TypeCard VALUES (58,'Elemental',1)
INSERT INTO TypeCard VALUES (59,'Elephant',1)
INSERT INTO TypeCard VALUES (60,'Elf',1)
INSERT INTO TypeCard VALUES (61,'Elk',1)
INSERT INTO TypeCard VALUES (62,'Eye',1)
INSERT INTO TypeCard VALUES (63,'Faerie',1)
INSERT INTO TypeCard VALUES (64,'Ferret',1)
INSERT INTO TypeCard VALUES (65,'Fish',1)
INSERT INTO TypeCard VALUES (66,'Flagbearer',1)
INSERT INTO TypeCard VALUES (67,'Fox',1)
INSERT INTO TypeCard VALUES (68,'Frog',1)
INSERT INTO TypeCard VALUES (69,'Fungus',1)
INSERT INTO TypeCard VALUES (70,'Gargoyle',1)
INSERT INTO TypeCard VALUES (71,'Germ',1)
INSERT INTO TypeCard VALUES (72,'Giant',1)
INSERT INTO TypeCard VALUES (73,'Gnome',1)
INSERT INTO TypeCard VALUES (74,'Goat',1)
INSERT INTO TypeCard VALUES (75,'Goblin',1)
INSERT INTO TypeCard VALUES (76,'God',1)
INSERT INTO TypeCard VALUES (77,'Golem',1)
INSERT INTO TypeCard VALUES (78,'Gorgon',1)
INSERT INTO TypeCard VALUES (79,'Graveborn',1)
INSERT INTO TypeCard VALUES (80,'Gremlin',1)
INSERT INTO TypeCard VALUES (81,'Griffin',1)
INSERT INTO TypeCard VALUES (82,'Hag',1)
INSERT INTO TypeCard VALUES (83,'Harpy',1)
INSERT INTO TypeCard VALUES (84,'Hellion',1)
INSERT INTO TypeCard VALUES (85,'Hippo',1)
INSERT INTO TypeCard VALUES (86,'Hippogriff',1)
INSERT INTO TypeCard VALUES (87,'Hormarid',1)
INSERT INTO TypeCard VALUES (88,'Homunculus',1)
INSERT INTO TypeCard VALUES (89,'Horror',1)
INSERT INTO TypeCard VALUES (90,'Horse',1)
INSERT INTO TypeCard VALUES (91,'Hound',1)
INSERT INTO TypeCard VALUES (92,'Human',1)
INSERT INTO TypeCard VALUES (93,'Hydra',1)
INSERT INTO TypeCard VALUES (94,'Hyena',1)
INSERT INTO TypeCard VALUES (95,'Illusion',1)
INSERT INTO TypeCard VALUES (96,'Imp',1)
INSERT INTO TypeCard VALUES (97,'Insect',1)
INSERT INTO TypeCard VALUES (98,'Jellyfish',1)
INSERT INTO TypeCard VALUES (99,'Juggernaut',1)
INSERT INTO TypeCard VALUES (100,'Kavu',1)
INSERT INTO TypeCard VALUES (101,'Kirin',1)
INSERT INTO TypeCard VALUES (102,'Kithkin',1)
INSERT INTO TypeCard VALUES (103,'Knight',1)
INSERT INTO TypeCard VALUES (104,'Kobold',1)
INSERT INTO TypeCard VALUES (105,'Kor',1)
INSERT INTO TypeCard VALUES (106,'Kraken',1)
INSERT INTO TypeCard VALUES (107,'Lamia',1)
INSERT INTO TypeCard VALUES (108,'Lammasu',1)
INSERT INTO TypeCard VALUES (109,'Leech',1)
INSERT INTO TypeCard VALUES (110,'Leviathan',1)
INSERT INTO TypeCard VALUES (111,'Lhurgoyf',1)
INSERT INTO TypeCard VALUES (112,'Licid',1)
INSERT INTO TypeCard VALUES (113,'Lizard',1)
INSERT INTO TypeCard VALUES (114,'Manticore',1)
INSERT INTO TypeCard VALUES (115,'Mercenary',1)
INSERT INTO TypeCard VALUES (116,'Merfolk',1)
INSERT INTO TypeCard VALUES (117,'Metathran',1)
INSERT INTO TypeCard VALUES (118,'Minion',1)
INSERT INTO TypeCard VALUES (119,'Minotaur',1)
INSERT INTO TypeCard VALUES (120,'Mole',1)
INSERT INTO TypeCard VALUES (121,'Monger',1)
INSERT INTO TypeCard VALUES (122,'Mongoose',1)
INSERT INTO TypeCard VALUES (123,'Monk',1)
INSERT INTO TypeCard VALUES (124,'Moonfolk',1)
INSERT INTO TypeCard VALUES (125,'Mutant',1)
INSERT INTO TypeCard VALUES (126,'Myr',1)
INSERT INTO TypeCard VALUES (127,'Mystic',1)
INSERT INTO TypeCard VALUES (128,'Naga',1)
INSERT INTO TypeCard VALUES (129,'Nautilus',1)
INSERT INTO TypeCard VALUES (130,'Nephilim',1)
INSERT INTO TypeCard VALUES (131,'Nightmare',1)
INSERT INTO TypeCard VALUES (132,'Nightstalker',1)
INSERT INTO TypeCard VALUES (133,'Ninja',1)
INSERT INTO TypeCard VALUES (134,'Noggle',1)
INSERT INTO TypeCard VALUES (135,'Nomad',1)
INSERT INTO TypeCard VALUES (136,'Nymph',1)
INSERT INTO TypeCard VALUES (137,'Octopus',1)
INSERT INTO TypeCard VALUES (138,'Ogre',1)
INSERT INTO TypeCard VALUES (139,'Ooze',1)
INSERT INTO TypeCard VALUES (140,'Orb',1)
INSERT INTO TypeCard VALUES (141,'Orc',1)
INSERT INTO TypeCard VALUES (142,'Orgg',1)
INSERT INTO TypeCard VALUES (143,'Ouphe',1)
INSERT INTO TypeCard VALUES (144,'Ox',1)
INSERT INTO TypeCard VALUES (145,'Oyster',1)
INSERT INTO TypeCard VALUES (146,'Pegasus',1)
INSERT INTO TypeCard VALUES (147,'Pentavite',1)
INSERT INTO TypeCard VALUES (148,'Pest',1)
INSERT INTO TypeCard VALUES (149,'Phelddagrif',1)
INSERT INTO TypeCard VALUES (150,'Phoenix',1)
INSERT INTO TypeCard VALUES (151,'Pincher',1)
INSERT INTO TypeCard VALUES (152,'Pirate',1)
INSERT INTO TypeCard VALUES (153,'Plant',1)
INSERT INTO TypeCard VALUES (154,'Praetor',1)
INSERT INTO TypeCard VALUES (155,'Prism',1)
INSERT INTO TypeCard VALUES (156,'Processor',1)
INSERT INTO TypeCard VALUES (157,'Rabbit',1)
INSERT INTO TypeCard VALUES (158,'Rat',1)
INSERT INTO TypeCard VALUES (159,'Rebel',1)
INSERT INTO TypeCard VALUES (160,'Reflection',1)
INSERT INTO TypeCard VALUES (161,'Rhino',1)
INSERT INTO TypeCard VALUES (162,'Rigger',1)
INSERT INTO TypeCard VALUES (249,'Rogue',1)
INSERT INTO TypeCard VALUES (163,'Sable',1)
INSERT INTO TypeCard VALUES (164,'Salamander',1)
INSERT INTO TypeCard VALUES (165,'Samurai',1)
INSERT INTO TypeCard VALUES (166,'Sand',1)
INSERT INTO TypeCard VALUES (167,'Saproling',1)
INSERT INTO TypeCard VALUES (168,'Satyr',1)
INSERT INTO TypeCard VALUES (169,'Scarecrow',1)
INSERT INTO TypeCard VALUES (170,'Scion',1)
INSERT INTO TypeCard VALUES (171,'Scorpion',1)
INSERT INTO TypeCard VALUES (172,'Scout',1)
INSERT INTO TypeCard VALUES (173,'Serf',1)
INSERT INTO TypeCard VALUES (174,'Serpent',1)
INSERT INTO TypeCard VALUES (175,'Shade',1)
INSERT INTO TypeCard VALUES (176,'Shaman',1)
INSERT INTO TypeCard VALUES (177,'Shapeshifter',1)
INSERT INTO TypeCard VALUES (178,'Sheep',1)
INSERT INTO TypeCard VALUES (179,'Siren',1)
INSERT INTO TypeCard VALUES (180,'Skeleton',1)
INSERT INTO TypeCard VALUES (181,'Slith',1)
INSERT INTO TypeCard VALUES (182,'Sliver',1)
INSERT INTO TypeCard VALUES (183,'Slug',1)
INSERT INTO TypeCard VALUES (184,'Snake',1)
INSERT INTO TypeCard VALUES (185,'Soldier',1)
INSERT INTO TypeCard VALUES (186,'Soltari',1)
INSERT INTO TypeCard VALUES (187,'Spawn',1)
INSERT INTO TypeCard VALUES (188,'Specter',1)
INSERT INTO TypeCard VALUES (189,'Spellshaper',1)
INSERT INTO TypeCard VALUES (190,'Sphinx',1)
INSERT INTO TypeCard VALUES (191,'Spider',1)
INSERT INTO TypeCard VALUES (192,'Spike',1)
INSERT INTO TypeCard VALUES (193,'Spirit',1)
INSERT INTO TypeCard VALUES (194,'Splinter',1)
INSERT INTO TypeCard VALUES (195,'Sponge',1)
INSERT INTO TypeCard VALUES (196,'Squid',1)
INSERT INTO TypeCard VALUES (197,'Squirrel',1)
INSERT INTO TypeCard VALUES (198,'Starfish',1)
INSERT INTO TypeCard VALUES (199,'Survivor',1)
INSERT INTO TypeCard VALUES (200,'Tetravite',1)
INSERT INTO TypeCard VALUES (201,'Thalakos',1)
INSERT INTO TypeCard VALUES (202,'Thopter',1)
INSERT INTO TypeCard VALUES (203,'Thrull',1)
INSERT INTO TypeCard VALUES (204,'Treefolk',1)
INSERT INTO TypeCard VALUES (205,'Triskelavite',1)
INSERT INTO TypeCard VALUES (206,'Troll',1)
INSERT INTO TypeCard VALUES (207,'Turtle',1)
INSERT INTO TypeCard VALUES (208,'Vampire',1)
INSERT INTO TypeCard VALUES (209,'Vedalken',1)
INSERT INTO TypeCard VALUES (210,'Viashino',1)
INSERT INTO TypeCard VALUES (211,'Volver',1)
INSERT INTO TypeCard VALUES (212,'Wall',1)
INSERT INTO TypeCard VALUES (213,'Warrior',1)
INSERT INTO TypeCard VALUES (214,'Weird',1)
INSERT INTO TypeCard VALUES (215,'Werewolf',1)
INSERT INTO TypeCard VALUES (216,'Whale',1)
INSERT INTO TypeCard VALUES (217,'Wizard',1)
INSERT INTO TypeCard VALUES (218,'Wolf',1)
INSERT INTO TypeCard VALUES (219,'Wolverine',1)
INSERT INTO TypeCard VALUES (220,'Wombat',1)
INSERT INTO TypeCard VALUES (221,'Worm',1)
INSERT INTO TypeCard VALUES (222,'Wraith',1)
INSERT INTO TypeCard VALUES (223,'Wurm',1)
INSERT INTO TypeCard VALUES (224,'Yeti',1)
INSERT INTO TypeCard VALUES (225,'Zombie',1)
INSERT INTO TypeCard VALUES (226,'Aqua',2)
INSERT INTO TypeCard VALUES (227,'Beast',2)
INSERT INTO TypeCard VALUES (228,'Beast-Warrior',2)
INSERT INTO TypeCard VALUES (229,'Dinoasur',2)
INSERT INTO TypeCard VALUES (230,'Divine-Beast',2)
INSERT INTO TypeCard VALUES (231,'Dragon',2)
INSERT INTO TypeCard VALUES (232,'Fairy',2)
INSERT INTO TypeCard VALUES (233,'Fiend',2)
INSERT INTO TypeCard VALUES (234,'Fish',2)
INSERT INTO TypeCard VALUES (235,'Insect',2)
INSERT INTO TypeCard VALUES (236,'Machine',2)
INSERT INTO TypeCard VALUES (250,'Plant',2)
INSERT INTO TypeCard VALUES (237,'Psychic',2)
INSERT INTO TypeCard VALUES (238,'Pyro',2)
INSERT INTO TypeCard VALUES (239,'Reptile',2)
INSERT INTO TypeCard VALUES (240,'Rock',2)
INSERT INTO TypeCard VALUES (241,'Sea Serpent',2)
INSERT INTO TypeCard VALUES (242,'Spellcaster',2)
INSERT INTO TypeCard VALUES (243,'Thunder',2)
INSERT INTO TypeCard VALUES (245,'Warrior',2)
INSERT INTO TypeCard VALUES (246,'Winged Beast',2)
INSERT INTO TypeCard VALUES (247,'Wyrm',2)
INSERT INTO TypeCard VALUES (248,'Zombie',2)

--Insert Card ('paperId', gameId, name, file name, rarity, cost
declare @cardIdentity int = 0;
INSERT INTO [Card] VALUES('war-253', 1, 'Island', 'war-253-island.jpg',1, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic
INSERT INTO CardCategory VALUES ( 2, @cardIdentity) --noncreature
INSERT INTO CardCategory VALUES (3, @cardIdentity) --land


INSERT INTO [Card] VALUES ('war-102', 1, 'Price of Betrayal','war-102-price-of-betrayal.jpg', 2, '{1B}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES (5, @cardIdentity) --sorcery
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic

INSERT INTO [Card] VALUES ('war-97', 1, 'Liliana, Dreadhorde General','war-97-liliana-dreadhorde-general.jpg', 4, '{4},{B},{B}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 2, @cardIdentity) --noncreature
INSERT INTO CardCategory VALUES (7, @cardIdentity) --planeswalker
INSERT INTO CardAttribute VALUES ( 2, @cardIdentity) --legendary

INSERT INTO [Card] VALUES ('war-186', 1, 'Bioessence Hydra','war-186-bioessence-hydra.jpg', 4, '{3},{G},{U}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 1, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic
INSERT INTO CardsTypeCard VALUES (93, @cardIdentity) --hydra 
INSERT INTO CardsTypeCard VALUES (125, @cardIdentity) --mutant

INSERT INTO [Card] VALUES ('m-19', 1,'Ajani, Adversary of Tyrants','m19-3-ajani-adversary-of-tyrants.jpg', 4, '{2},{W},{W}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 2, @cardIdentity) --noncreature
INSERT INTO CardCategory VALUES ( 7, @cardIdentity) --planeswalker
INSERT INTO CardAttribute VALUES ( 2, @cardIdentity) --legendary

INSERT INTO [Card] VALUES ('jou-145', 1,'Ajani, Mentor of Heroes','jou-145-ajani-mentor-of-heroes.jpg', 4, '{3},{G},{W}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 2, @cardIdentity) --noncreature
INSERT INTO CardCategory VALUES ( 7, @cardIdentity) --planeswalker
INSERT INTO CardAttribute VALUES ( 2, @cardIdentity) --legendary

INSERT INTO [Card] VALUES ('ima-204', 1,'Lightning Helix ','ima-204-lightning-helix.jpg', 2, '{3},{R},{W}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 2, @cardIdentity) --noncreature
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic
INSERT INTO CardAttribute VALUES ( 4, @cardIdentity) --instant

INSERT INTO [Card] VALUES ('c16-26', 1,'Akiri, Line-Slinger','c16-26-akiri-line-slinger.jpg', 3, '{},{R},{W}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 1, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 2, @cardIdentity) --legendary
INSERT INTO CardsTypeCard VALUES (105, @cardIdentity) --kor 
INSERT INTO CardsTypeCard VALUES (185, @cardIdentity) --soldier
INSERT INTO CardsTypeCard VALUES (3, @cardIdentity) --ally

INSERT INTO [Card] VALUES ('gtc-144', 1,'Aurelia''s Fury','gtc-144-aurelia-s-fury.jpg', 4, '{X},{R},{W}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 4, @cardIdentity) --instant
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic

INSERT INTO [Card] VALUES ('rna-206', 1,'Sharktocrab','rna-206-sharktocrab.jpg', 2, '{2},{G},{B}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 1, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 1, @cardIdentity) --basic
INSERT INTO CardsTypeCard VALUES (65, @cardIdentity) --fish
INSERT INTO CardsTypeCard VALUES (137, @cardIdentity) --Octopus
INSERT INTO CardsTypeCard VALUES (39, @cardIdentity) --Crab

INSERT INTO [Card] VALUES ('33396948', 2,'Exodia the Forbidden One','33396948-exodia.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 10, @cardIdentity) --dark
INSERT INTO CardsTypeCard VALUES (242, @cardIdentity) -- Spellcaster

INSERT INTO [Card] VALUES ('74677422', 2,'Red-Eyes B. Dragon','74677422-Red-Eyes B. Dragon.jpg', 5, '{2}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 10, @cardIdentity) --dark
INSERT INTO CardsTypeCard VALUES (231, @cardIdentity) --dragon 

INSERT INTO [Card] VALUES ('6442944', 2,'Kaiser Sea Snake','6442944-Kaiser Sea Snake.jpg', 5, '{2}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 6, @cardIdentity) --water
INSERT INTO CardsTypeCard VALUES (241, @cardIdentity) --sea-serpent

INSERT INTO [Card] VALUES ('73359475', 2,'Noble Knight Peredur','73359475-Noble Knight Peredur.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 9, @cardIdentity) --light
INSERT INTO CardsTypeCard VALUES (245, @cardIdentity) --warrior 

INSERT INTO [Card] VALUES ('12213463', 2,'White Rose Dragon','12213463-White Rose Dragon.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 10, @cardIdentity) --dark
INSERT INTO CardsTypeCard VALUES (231, @cardIdentity) --dragon

INSERT INTO [Card] VALUES ('63948258', 2,'Submarine Frog','63948258-Submarine Frog.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 6, @cardIdentity) --water
INSERT INTO CardsTypeCard VALUES (226, @cardIdentity) --aqua

INSERT INTO [Card] VALUES ('39004808', 2,'Root Water','39004808-Root-Water.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 8, @cardIdentity) --creature
INSERT INTO CardAttribute VALUES ( 6, @cardIdentity) --water
INSERT INTO CardsTypeCard VALUES (234, @cardIdentity) --fish 

INSERT INTO [Card] VALUES ('94192409', 2,'Compulsory Evacuation Device','94192409-Compulsory Evacuation Device.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 10, @cardIdentity) --trap

INSERT INTO [Card] VALUES ('41234315', 2,'Fake Explosion','41234315-Fake Explosion.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 10, @cardIdentity) --trap 

INSERT INTO [Card] VALUES ('95784434', 2,'Molting Escape','95784434-Molting Escape.jpg', 5, '{}')
set @cardIdentity = SCOPE_IDENTITY()
INSERT INTO CardCategory VALUES ( 9, @cardIdentity) --spell

--insert user ('username','password', role)
declare @userIdentity int = 1;
INSERT INTO [User] VALUES ('noobkiller69','yolopsw', 'yolo@email.com', 2)
set @userIdentity = SCOPE_IDENTITY()
--insert usercard (@userIdentity, cardIdentity, copies, gameId)
INSERT INTO UserCard VALUES (@userIdentity, 4, 3, 1)
INSERT INTO UserCard VALUES (@userIdentity, 5, 2, 1)
INSERT INTO UserCard VALUES (@userIdentity, 1, 4, 1)
INSERT INTO UserCard VALUES (@userIdentity, 12, 3, 2)
INSERT INTO [User] VALUES ('xXGamerXx', 'gamerpsw', 'gamer@email.com', 2)
set @userIdentity = SCOPE_IDENTITY()
INSERT INTO UserCard VALUES (@userIdentity, 4, 3, 1)
INSERT INTO UserCard VALUES (@userIdentity, 4, 3, 1)
INSERT INTO UserCard VALUES (@userIdentity, 4, 3, 1)

--insert deck (gameId, 0, 'name', userId)
declare @deckIdentity int = 1;
INSERT INTO [Deck] VALUES (1, 60, 'mono sharktocrab', 1)
set @deckIdentity = SCOPE_IDENTITY()
--insert cards into deck (@deckIdentity, cardId, copies, gameId)
INSERT INTO DeckCard VALUES (@deckIdentity, 10, 60, 1)
