import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from 'src/app/models/card.model';
import {style, state, animate, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-card',
  templateUrl: 'card.component.html',
  styles: [],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity:0}),
        animate(300, style({opacity:1})) 
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(150, style({opacity:0})) 
      ])
    ])
  ]

})
export class CardComponent implements OnInit {

  @Input() card: Card;
  @Output() updateEvent = new EventEmitter<Card>();
  @Input() showDetail: boolean;
  @Output() detailEvent = new EventEmitter<Card>();
  constructor() {

  }

  ngOnInit() {
  }

  add() {
    this.card.copies++;
    this.updateEvent.emit(this.card);
  }

  sub() {
    if (this.card.copies > 0)
      this.card.copies--;
    this.updateEvent.emit(this.card);
  }

  toggleDetail() {
    if (this.showDetail) { //unfocus card
      this.detailEvent.emit(new Card());
    } else { //focus card
      this.detailEvent.emit(this.card);
    }
  }
}
