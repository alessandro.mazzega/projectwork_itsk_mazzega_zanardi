﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CardCollector.Logic;
using CardCollector.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CardCollector.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class TypeCardController : ControllerBase {// GET: api/TypeCard
        [HttpGet]
        public List<TypeCard> Get() {
            return GenericDB.GetFromDb<TypeCard>("TypeCard");
        }

        // GET: api/TypeCard/5
        [HttpGet("{id}")]
        public TypeCard Get(int id) {
            return GenericDB.GetFromDb<TypeCard>("TypeCard", id);
        }

        // POST: api/TypeCard
        [HttpPost]
        public IActionResult Post() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // PUT: api/TypeCard/5
        [HttpPut("{id}")]
        public IActionResult Put() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete() {
            return StatusCode(403, "Operation denied, you lack the necessary authority");
        }
    }
}
