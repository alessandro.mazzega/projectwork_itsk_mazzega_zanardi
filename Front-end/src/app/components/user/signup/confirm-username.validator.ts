import { FormGroup } from '@angular/forms';
import { UserService } from 'src/app/services/services';

// custom validator to check that the value is not already used by another user
export function UsernameAvailable() {
    return async (formGroup: FormGroup) => {
        const control = formGroup.controls["username"];
        console.log(control.value);
        let userSrv: UserService;
        console.log("sending request");
		let usernameTaken = await userSrv.UsernameAvailable(control.value).toPromise();
        // set error on control if validation fails
        if (usernameTaken) {
            console.log("taken");
            control.setErrors({ usernameTaken: true });
        } else {
            console.log("not taken");
            control.setErrors(null);
        }
    }
}

