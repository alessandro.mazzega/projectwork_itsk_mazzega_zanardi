﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    ///<summary>
    ///Class used to represent a N:N relation between the entities Deck and Card. 
    ///</summary>
    public class DeckCard {
        public int DeckCardId { get; set; }
        public int DeckId { get; set; }
        public int CardId { get; set; }
        public int Copies { get; set; }
        public int GameId { get; set; }
        public Card Card { get; set; }
        public Game Game { get; set; }
    }
}
