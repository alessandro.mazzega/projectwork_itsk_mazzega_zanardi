﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CardCollector.Models {
    //this class is not need in the new implementation and it's here only as a backup
    //class used to represent a N:N relation between the entities User and Deck. The relation identifies a deck owned by a certain player
    public class UserDeck {
        public int UserDeckId { get; set; }
        public int UserId { get; set; }
        public int DeckId { get; set; }
        public User User { get; set; }
        public Deck Deck { get; set; }
    }
}
